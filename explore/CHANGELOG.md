# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

*For each release, use the following sub-sections:*

- *Added (for new features)*
- *Changed (for changes in existing functionality)*
- *Deprecated (for soon-to-be removed features)*
- *Removed (for now removed features)*
- *Fixed (for any bug fixes)*
- *Security (in case of vulnerabilities)*

## [production-release-august-2024] - 2024/08/01
### Changed
* Hide L3 & L4 from FoS (Fields of Science page, Detailed page, Advanced search page)
* Search page: Reordered buttons - moved compact results button before download button

## [production-release-july-2024] - 2024/07/11
### Changed
* Parse instances new info (to delete deletedbyinference records)
* Detailed page: Remove subjects by vocabulary section and show all subjects under keywords

### Fixed
* Updated vocabulary for Fields of Science level 4
* Linking: Do not add unidentified code in queries - missing irish funders

### Added
* Funders page: More information in cards (i.e. country, registered) 
* Funders page: New sorting options by research products & projects
* Links between Research products and Data sources
* Design of compact search results

## [production-release-june-2024] - 2024/06/04
### Changed
* Filter out unidentified projects from the search page and related tabs in detailed pages
* Updated link functionality for funding: search either by selecting funder or directly for projects

### Fixed
* Search results: adjust message when >= 1000 relations in the metadata line
* Fixed message on "Related Data Sources" tab of Data source detailed page
* Query 100 top values when a value of the filter is selected (bug when selected not in top 7 values)

## [production-release-may-2024] - 2024/05/23
### Changed
* Restored in search pages: number of results & selected keyword
* Performance improvements: onPush Strategy for checking when there are content changes in search results
* Updated color of filter labels & set color of "x" on filter labels same as the text
* Updated cards in funders page

### Fixed
* Filters in claims (projects) always loading
* z-index issue in dropdown (ORCID) inside modal (authors)
* Close dropdown of ORCID author when clicking on the search button
* Replaced /projects search endpoint with /resources2?type=projects in project claims

### Added
* Link directly with funding (unidentified projects)
* Display organization persistent identifiers (search results & detailed page)
* Search & Advanced search organizations by pid
* Display full organization names in search organizations page
* Added level4 FoS in fos.json vocabulary

## [production-release-march-2024] - 2024/03/04
### Changed
* Performance improvements
* Search pages: Access Routes, impact-based indicators, usage counts: Open drops on click, instead of hover
* Linking: change Datacite api from /works to /dois
* Updated funding text in footer
* Removed tooltips from search results
* UI updates on search pages 
  * Paging only at the bottom under the search results
  * Removed number of results
  * Removed results per page option

### Fixed
* z-index issue in dropdowns inside modals
* Change search tab from menu
* Requests in bulk DOIs upload in linking
* View all for search filters
* Research products detailed page: Provenance label in funded by

### Added
* Full-Text link in Research products

## [production-release-february-2024] - 2024/02/12
### Changed
* Remove "open access" as preselected filter
* Rounded numbers in search tabs
* Home page: Performance improvements on images loading
* Home page, Search page, Funders page: Performance improvements on services requests

### Fixed
* Filtering out in funding levels according to selected funder when querying 100 values
* Filters in claims & in organization landing > projects tab were always loading
* Research products landing page: Added providers in versions
* SEO fixes in landing pages

## [production-release-january-2024] - 2024/02/01
### Changed
* Updated FoS search filter - do not display code
* Removed "BETA" indication from FoS
* Access search filter: Display always all the predefined values
* Filters: remove number of values
* Filters: Get 7 value (display 6) and 100 on "view more" click
* My claims: remove  urls for pending claims
* Improve queries for search organizations & data sources

### Fixed
* Search query with input full ORCID url or just ORCID id
* In search, added also in count query for data sources the datasource pid check
* Landing: min-height needed in metrics box

### Added
* Linking: responsive in mobile

## [production-release-november-2023-v2] - 2023/11/28
### Added
* Add sorting options with impact factors in search results page

### Changed
* Data source landing: get collected full texts from new stats API
* Updated parsing of measures - in results: views/downloads per data source

### Fixed
* Updated query for funders number in Funders page
* Updated query for funders number is first page 
* Advanced Search Results: funder field to return all funders 
* Advanced Search Projects: funder field to return all funders

## [production-release-november-2023] - 2023/11/09
### Added
* Missing funders' logos locally in Explore assets

### Changed
* Deposit search page: UI improvement in “Go to repository” button

### Fixed
* Reverted version of UIkit back to version 3.16.24 - previous version caused bug with dropdowns

## [production-release-october-2023] - 2023/10/18
### Added
* Search page: Add view more option on overflow (e.g authors, partners, subjects, projects)

### Changed
* Upgrade to Angular 16
* New Claims API
* New Funders Page

### Fixed
* Reverted version of UIkit to version 3.16.14 because of parallax
* A persistent identifier can resolve in more than one pid types

## [production-release-september-2023] - 2023/09/25
### FIXED
* Error on grant access in ORCID Search & Link Wizard 
* Added missing id in linking from the project landing
* Added check in search data sources parsing

## [production-release-august-2023] - 2023/08/25
### Added
* Search mobile: Added actions
* Detailed pages - mobile: Added search-bar
* Mobile: new drops and dropdowns

### Changed
* Linking: Redesign of results view
* Advanced search form: Add autocomplete input for eosc subject field
* Detailed pages - mobile: UI updates & improvements

### Fixed
* Advanced search and detailed page
* Added a necessary check in html of detailed pages

## [production-release-may-2023] - 2023/05/19
### Added
* Detailed research products page: Added suggest modals for Sustainable Development Goals and Fields of Science
* Get research products, projects & data sources usage counts from index

### Changed
* Redesign of detailed pages and mobile version

### Fixed
* Fix path for fos/sdg vocabularies in advanced search
* Search page improvements
* UI updates & improvements in search results and detailed pages (minor)

## [production-release-january-2023] - 2023/01/18
### FIXED
* Contact us form: Email body was either empty or incomplete
* Contact us page: After sending an email, clear recaptcha


## [production-release-december-2022] - 2022/12/21
### Added
* Link to blog in Sustainable Development Goals page

### Changed
* SEO updates and mobile menu

### Fixed
* Linking: Changed Crossref requests for bulk uploaded DOIs
* Medium screens: not sticky navbar causing error

## [production-release-november-2022] - 2022/11/30
### Added
* EOSC subjects from eoscifguidelines field & Advanced field "EOSC Subject"
* New Contact us page

### Changed
* Upgrade to Angular 14
* Refinement of filters
* Advanced search form: UI updates and smooth scrolling
* FoS and deposit pages updates
* Bip Finder: Updated parsing and labels for Bip Finder scores & updated link to BiP Finder