import 'zone.js/node';

import { ngExpressEngine } from '@nguniversal/express-engine';
import * as express from 'express';
import * as compression from 'compression';
import { join } from 'path';

import { AppServerModule } from './src/main.server';
import { APP_BASE_HREF } from '@angular/common';
import { existsSync } from 'fs';
import * as prom from "prom-client";
import {routes} from "./routes";
import {REQUEST, RESPONSE} from "./src/app/openaireLibrary/utils/tokens";

// The Express app is exported so that it can be used by serverless Functions.
export function app() {
  const server = express();
  server.use(compression());
  const distFolder = join(process.cwd(), 'dist/explore/browser');
  const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';
  
  const register = new prom.Registry();
  
  const responses = new prom.Counter({
    name: 'portal_http_responses_total',
    help: 'A counter for portal response codes for every existed or not existed pages.',
    labelNames: ['route', 'code'],
    registers: [register]
  });
  
  const histogram = new prom.Histogram({
    name: 'portal_http_request_duration_seconds',
    help: 'A Histogram for a portal. Providing information about page visits and load latency in seconds.',
    labelNames: ['route'],
    registers: [register],
    buckets: [0.1, 0.2, 0.5, 1, 2]
  });
  
  // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
  server.engine('html', ngExpressEngine({
    bootstrap: AppServerModule,
    inlineCriticalCss: false
  }));

  server.set('view engine', 'html');
  server.set('views', distFolder);

  // Example Express Rest API endpoints
  // server.get('/api/**', (req, res) => { });
  // Serve static files from /browser
  server.get('*.*', express.static(distFolder, {
    maxAge: '1y'
  }));
  
  server.get('/metrics', (req, res) => {
    res.set('Content-Type', register.contentType);
    res.end(register.metrics());
  });

  server.get('/health-check', async (_req, res, _next) => {
    var uptime = process.uptime();
    const date = new Date(uptime*1000);
    const days = date.getUTCDate() - 1,
      hours = date.getUTCHours(),
      minutes = date.getUTCMinutes(),
      seconds = date.getUTCSeconds(),
      milliseconds = date.getUTCMilliseconds();


    const healthcheck = {
      uptime: days + " days, " + hours + " hours, " + minutes + " minutes, " + seconds + " seconds, " + milliseconds + " milliseconds",
      message: 'OK',
      timestamp: new Date()
    };
    try {
      res.send(healthcheck);
    } catch (error) {
      healthcheck.message = error;
      res.status(503).send();
    }
  });

  // All regular routes use the Universal engine
  server.get('*', (req, res) => {
    if (routes.indexOf(req.path) !== -1) {
      const end = histogram.startTimer({route: req.path});
      res.render(indexHtml, {
          req, providers: [
            {
              provide: APP_BASE_HREF,
              useValue: req.baseUrl
            },
            {
              provide: REQUEST, useValue: (req)
            },
            {
              provide: RESPONSE, useValue: (res)
            }
          ]
        }
      );
      res.on('finish', function () {
        responses.inc({route: req.path, code: res.statusCode});
        end();
      });
    } else {
      res.render(indexHtml, {
          req, providers: [
            {
              provide: APP_BASE_HREF,
              useValue: req.baseUrl
            },
            {
              provide: REQUEST, useValue: (req)
            },
            {
              provide: RESPONSE, useValue: (res)
            }
          ]
        }
      );
      res.on('finish', function () {
        responses.inc({route: '**', code: res.statusCode});
      });
    }
  });

  return server;
}

function run() {
  const port = process.env.PORT || 4000;

  // Start up the Node server
  const server = app();
  server.listen(port, () => {
    console.log(`Node Express server listening on http://localhost:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
