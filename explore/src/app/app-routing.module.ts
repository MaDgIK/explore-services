import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageURLResolverComponent} from "./openaireLibrary/utils/pageURLResolver.component";
import {SdgModule} from "./openaireLibrary/sdg/sdg.module";
import {ErrorPageComponent} from "./openaireLibrary/error/errorPage.component";

const routes: Routes = [
  // Other Pages
  {path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule), data: {hasStickyHeaderOnMobile: true}},
  {path: 'home', redirectTo: '', pathMatch: 'full'},
	{path: 'sdgs', loadChildren: () => import('./openaireLibrary/sdg/sdg.module').then(m => m.SdgModule)},
	{path: 'fields-of-science', loadChildren: () => import('./openaireLibrary/fos/fos.module').then(m => m.FosModule), data: {extraOffset: 100}},
  {path: 'funders', loadChildren: () => import('./funders/funders.module').then(m => m.FundersModule)},
	{path: 'contact-us', loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule), data: {hasQuickContact: false}},
  // ORCID Pages
  {path: 'orcid', loadChildren: () => import('./openaireLibrary/orcid/orcid.module').then(m => m.OrcidModule)},
  {path: 'my-orcid-links', loadChildren: () => import('./openaireLibrary/orcid/my-orcid-links/myOrcidLinks.module').then(m => m.MyOrcidLinksModule)},
  // Landing Pages
  {path: 'search/result', loadChildren: () => import('./openaireLibrary/landingPages/result/resultLanding.module').then(m => m.ResultLandingModule), data: {hasQuickContact: false, hasMenuSearchBar: true, type: 'result', community: 'openaire'}},
  {path: 'search/publication', loadChildren: () => import('./openaireLibrary/landingPages/result/resultLanding.module').then(m => m.ResultLandingModule), data: {hasQuickContact: false, hasMenuSearchBar: true, type: 'publication', community: 'openaire'}},
  {path: 'search/dataset', loadChildren: () => import('./openaireLibrary/landingPages/result/resultLanding.module').then(m => m.ResultLandingModule), data: {hasQuickContact: false, hasMenuSearchBar: true, type: 'dataset', community: 'openaire'}},
  {path: 'search/software', loadChildren: () => import('./openaireLibrary/landingPages/result/resultLanding.module').then(m => m.ResultLandingModule), data: {hasQuickContact: false, hasMenuSearchBar: true, type: 'software', community: 'openaire'}},
  {path: 'search/other', loadChildren: () => import('./openaireLibrary/landingPages/result/resultLanding.module').then(m => m.ResultLandingModule), data: {hasQuickContact: false, hasMenuSearchBar: true, type: 'orp', community: 'openaire'}},
  {path: 'search/project', loadChildren: () => import('./openaireLibrary/landingPages/project/project.module').then(m => m.ProjectModule), data: {hasQuickContact: false, hasMenuSearchBar: true, community: 'openaire'}},
  {
    path: 'search/dataprovider',
    loadChildren: () => import('./openaireLibrary/landingPages/dataProvider/dataProvider.module').then(m => m.DataProviderModule),
		data: {hasQuickContact: false, hasMenuSearchBar: true, community: 'openaire'}
  },
  {
    path: 'search/organization',
    loadChildren: () => import('./openaireLibrary/landingPages/organization/organization.module').then(m => m.OrganizationModule),
		data: {hasQuickContact: false, hasMenuSearchBar: true, community: 'openaire'}
  },

  // Search Pages
  {
    path: 'search/find', loadChildren: () => import('./searchPages/find/libSearch.module').then(m => m.LibMainSearchModule)
  },
  {path: 'search/find/publications', component: PageURLResolverComponent},
  {path: 'search/find/datasets', component: PageURLResolverComponent},
  {path: 'search/find/software', component: PageURLResolverComponent},
  {path: 'search/find/other', component: PageURLResolverComponent},
  {
    path: 'search/find/:entity', loadChildren: () => import('./searchPages/find/libSearch.module').then(m => m.LibMainSearchModule)
  },

  // Advanced Search Pages
  {
    path: 'search/advanced/research-outcomes',
    loadChildren: () => import('./searchPages/advanced/searchResearchResults.module').then(m => m.OpenaireAdvancedSearchResearchResultsModule)
  },
  {path: 'search/advanced/publications', component: PageURLResolverComponent},
  {path: 'search/advanced/datasets', component: PageURLResolverComponent},
  {path: 'search/advanced/software', component: PageURLResolverComponent},
  {path: 'search/advanced/other', component: PageURLResolverComponent},
  {
    path: 'search/advanced/organizations',
    loadChildren: () => import('./searchPages/advanced/advancedSearchOrganizations.module').then(m => m.LibAdvancedSearchOrganizationsModule)
  },
  {
    path: 'search/advanced/dataproviders',
    loadChildren: () => import('./searchPages/advanced/advancedSearchDataProviders.module').then(m => m.LibAdvancedSearchDataProvidersModule)
  },
  {
    path: 'search/advanced/projects',
    loadChildren: () => import('./searchPages/advanced/advancedSearchProjects.module').then(m => m.LibAdvancedSearchProjectsModule)
  },
  // Search Content Providers Pages
  {
    path: 'search/content-providers',
    loadChildren: () => import('./searchPages/dataProviders/compatibleDataProviders.module').then(m => m.LibCompatibleDataProvidersModule)
  },
  {path: 'search/content-providers-table', redirectTo: 'search/content-providers', pathMatch: 'full'},
  {
    path: 'search/entity-registries',
    loadChildren: () => import('./searchPages/dataProviders/entityRegistries.module').then(m => m.LibEntityRegistriesModule)
  },
  {path: 'search/entity-registries-table', redirectTo: 'search/entity-registries', pathMatch: 'full'},
  {path: 'search/journals', loadChildren: () => import('./searchPages/dataProviders/journals.module').then(m => m.LibJournalsModule)},
  {path: 'search/journals-table', redirectTo: 'search/journals', pathMatch: 'full'},
  // Deposit Pages
  {path: 'participate/deposit-datasets', redirectTo: 'participate/deposit/learn-how', pathMatch: 'full'},
  {path: 'participate/deposit-datasets-result', redirectTo: 'participate/deposit/learn-how', pathMatch: 'full'},
  {path: 'participate/deposit-subject-result', redirectTo: 'participate/deposit/learn-how', pathMatch: 'full'},
  {path: 'participate/deposit-publications', redirectTo: 'participate/deposit/learn-how', pathMatch: 'full'},
  {path: 'participate/deposit-publications-result', redirectTo: 'participate/deposit/learn-how', pathMatch: 'full'},
  {path: 'participate/deposit/learn-how', loadChildren: () => import('./deposit/deposit.module').then(m => m.LibDepositModule)},
  {
    path: 'participate/deposit/search',
    loadChildren: () => import('./deposit/searchDataprovidersToDeposit.module').then(m => m.LibSearchDataprovidersToDepositModule)
  },
  // Linking Pages
  {path: 'myclaims', loadChildren: () => import('./claims/myClaims/myClaims.module').then(m => m.LibMyClaimsModule)},
  {path: 'claims', loadChildren: () => import('./claims/claimsAdmin/claimsAdmin.module').then(m => m.LibClaimsAdminModule)},
  {path: 'participate/claim', loadChildren: () => import('./claims/linking/linkingGeneric.module').then(m => m.LibLinkingGenericModule)},
  {
    path: 'participate/direct-claim',
    loadChildren: () => import('./claims/directLinking/directLinking.module').then(m => m.LibDirectLinkingModule)
  },
  // { path: 'claims-project-manager', loadChildren: './claims/claimsByToken/claimsByToken.module#LibClaimsByTokenModule'},
  // help pages - do not exist in Admin portal/api/db
  {path: 'reload', loadChildren: () => import('./openaireLibrary/reload/reload.module').then(m => m.ReloadModule)},
  {path: 'user-info', loadChildren: () => import('./openaireLibrary/login/user.module').then(m => m.UserModule)},
  {path: 'error', component: ErrorPageComponent},
  {path: '**', pathMatch: 'full', component: ErrorPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: "reload"
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
