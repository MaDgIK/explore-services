import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EnvProperties} from './openaireLibrary/utils/properties/env-properties';
import {MenuItem} from './openaireLibrary/sharedComponents/menu';
import {EnvironmentSpecificService} from './openaireLibrary/utils/properties/environment-specific.service';
import {Session, User} from './openaireLibrary/login/utils/helper.class';
import {UserManagementService} from "./openaireLibrary/services/user-management.service";
import {ConfigurationService} from "./openaireLibrary/utils/configuration/configuration.service";
import {Header} from "./openaireLibrary/sharedComponents/navigationBar.component";
import {Subscriber} from "rxjs";
import {Meta} from "@angular/platform-browser";
import {properties} from "../environments/environment";
import {SmoothScroll} from "./openaireLibrary/utils/smooth-scroll";
import {SEOService} from "./openaireLibrary/sharedComponents/SEO/SEO.service";
import {OpenaireEntities} from "./openaireLibrary/utils/properties/searchFields";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {QuickContactComponent} from './openaireLibrary/sharedComponents/quick-contact/quick-contact.component';
import {EmailService} from './openaireLibrary/utils/email/email.service';
import {Composer} from "./openaireLibrary/utils/email/composer";
import {AlertModal} from './openaireLibrary/utils/modal/alert';
import {NotificationHandler} from "./openaireLibrary/utils/notification-handler";
import {QuickContactService} from './openaireLibrary/sharedComponents/quick-contact/quick-contact.service';
import {LayoutService} from './openaireLibrary/dashboard/sharedComponents/sidebar/layout.service';
import {ISVocabulariesService} from "./openaireLibrary/utils/staticAutoComplete/ISVocabularies.service";

@Component({
  selector: 'app-root',
  template: `
    <navbar *ngIf="properties && header" [header]="header" [portal]="properties.dashboard" [properties]=properties
            [onlyTop]=false [user]="user"
            [communityId]="properties.adminToolsCommunity" [userMenuItems]=userMenuItems [menuItems]=menuItems></navbar>
    <div *ngIf="loading">
        <loading [full]="true"></loading>
    </div>
    <schema2jsonld *ngIf="properties" [URL]="properties.domain+properties.baseLink"
                   [logoURL]="properties.domain+properties.baseLink+'/assets/common-assets/logo-services/explore/main.svg'"
                   type="home"
                   name="OpenAIRE | Find and Share research"
                   description="OpenAIRE Explore: Over 100M of research deduplicated, 170K research software, 11M research data. One of the largest open scholarly records collection worldwide."></schema2jsonld>
    <div [class.uk-hidden]="loading" [ngClass]="rootClass">
      <div id="modal-container"></div>
<!--      <span class="js-upload" uk-form-custom>-->
<!--        <input id="exampleInputFile" class="uk-width-medium" type="file" (change)="fileChangeEvent($event)"/>-->
<!--        <span class="uk-link " style="text-decoration: underline;">Parse NEW FOS</span>-->
<!--      </span>-->

<!--      <a (click)="checkDifferences()" class="uk-margin-left uk-link ">Check if different FOS codes/labels</a>-->
      <main>
        <router-outlet></router-outlet>
      </main>
    </div>
    <cookie-law *ngIf="isClient" position="bottom">
      <span class="uk-visible@m">OpenAIRE uses cookies in order to function properly.<br>
      Cookies are small pieces of data that websites store in your browser to allow us to give you the best browsing
        experience possible.</span>
      By using the OpenAIRE portal you accept our use of cookies. <a
        href="https://www.openaire.eu/privacy-policy#cookies" target="_blank"> Read more <span class="uk-icon">
              <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" icon="chevron-right"
                   ratio="1"><polyline fill="none" stroke="#000" stroke-width="1.03" points="7 4 13 10 7 16"></polyline></svg>
              </span></a>
    </cookie-law>
    <bottom id="bottom" #bottom *ngIf="isClient && properties" [properties]="properties"></bottom>
		<quick-contact #quickContact *ngIf="bottomNotIntersecting && displayQuickContact && showQuickContact && contactForm" (sendEmitter)="send($event)"
			[contactForm]="contactForm" [sending]="sending" [contact]="'Help'" class="uk-visible@m"></quick-contact>
		<modal-alert #modal [overflowBody]="false"></modal-alert>
  `
})
export class AppComponent implements AfterViewInit {
  loading: boolean = false;
  rootClass: string;
  isClient: boolean = false;
  userMenuItems: MenuItem[] = [];
  menuItems: MenuItem [] = [];
  feedbackmail: string;
  properties: EnvProperties = properties;
  user: User;
  header: Header;
  /* Contact */
  public showQuickContact: boolean;
  public bottomNotIntersecting: boolean;
  public displayQuickContact: boolean;  // intersecting with specific section in home page
	public contactForm: FormGroup;
	public sending: boolean = false;
	@ViewChild('quickContact') quickContact: QuickContactComponent;
	@ViewChild('modal') modal: AlertModal;
	@ViewChild('bottom', {read: ElementRef}) bottom: ElementRef;

  subscriptions = [];
  
  constructor(private  route: ActivatedRoute, private propertiesService: EnvironmentSpecificService,
              private router: Router, private userManagementService: UserManagementService, private smoothScroll: SmoothScroll,
              private configurationService: ConfigurationService, private _meta: Meta, private seoService: SEOService,
							private emailService: EmailService, private fb: FormBuilder, private quickContactService: QuickContactService,
							private layoutService: LayoutService, private cdr: ChangeDetectorRef,
              private ISVocabulariesService: ISVocabulariesService) {
  }

  // fileChangeEvent(fileInput: any) {
  //   let file = <Array<File>>fileInput.target.files;
  //   this.parseNewFos(file);
  // }
  //
  // makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
  //   return new Promise<void>((resolve, reject) => {
  //     const formData: any = new FormData();
  //     const xhr = new XMLHttpRequest();
  //     for (let i = 0; i < files.length; i++) {
  //       formData.append("uploads[]", files[i], files[i].name);
  //     }
  //     xhr.onreadystatechange = function () {
  //       if (xhr.readyState == 4) {
  //         if (xhr.status == 200) {
  //           resolve(xhr.response);
  //         } else {
  //           reject(xhr.response);
  //         }
  //       }
  //     }
  //
  //     xhr.open("POST", url, true);
  //     xhr.send(formData);
  //   });
  // }
  //
  // parseNewFos(file: Array<File>) {
  //   let fos = null;
  //     this.makeFileRequest(this.properties.utilsService + '/upload', [], file).then((result) => {
  //       this.ISVocabulariesService.getFos(properties).subscribe(fosRes => {
  //         fos = fosRes['fos'];
  //         for(let fieldL1 of fos) {
  //           if(fieldL1.children != null) {
  //             for (let fieldL2 of fieldL1.children) {
  //               if(fieldL2.children != null) {
  //                 for (let fieldL3 of fieldL2.children) {
  //                   if(fieldL3.children != null)
  //                   fieldL3.children = [];
  //                 }
  //               }
  //             }
  //           }
  //         }
  //         // console.log(fos);
  //         let setOfCodes: Set<string> = new Set<string>();
  //         let duplicates = new Map<string, string[]>;
  //         const rows = (result as any).split('\n');  // I have used space, you can use any thing.
  //         let added = false;
  //         for (let i = 0; i < (rows.length); i++) {
  //           if (rows[i] && rows[i] != null && rows[i] != "") {
  //             added = false;
  //             let code = rows[i].split(' ')[0];
  //             if(setOfCodes.has(code)) {
  //               console.log("SAME CODE!!!!!!!!");
  //             } else {
  //               setOfCodes.add(code);
  //             }
  //             let label = rows[i].substring(code.length+1, rows[i].length);
  //             // console.log(rows[i]);
  //             let firstLevel = rows[i].substring(0, 2);
  //             let secondLevel = rows[i].substring(0, 4);
  //             let thirdLevel = rows[i].substring(0, 6);
  //             for(let fieldL1 of fos) {
  //               if(fieldL1.code == firstLevel) {
  //                 for(let fieldL2 of fieldL1.children) {
  //                   if(fieldL2.code == secondLevel) {
  //                     for(let fieldL3 of fieldL2.children) {
  //                       if(fieldL3.code == thirdLevel) {
  //                         if(fieldL3.children == null) {
  //                           fieldL3.children = [];
  //                         }
  //                         fieldL3.children.push({
  //                           "code": code,
  //                           "id": rows[i],
  //                           "label": label,
  //                           "level": 4
  //                         })
  //                         added = true;
  //                         break;
  //                       }
  //                     }
  //                     if(!added && (!fieldL2.children || fieldL2.children.filter(field => field.code == thirdLevel).length == 0)) {
  //                       console.log("Level 3 not found for "+rows[i]);
  //                     }
  //                     break;
  //                   }
  //                 }
  //                 if(!added && (!fieldL1.children || fieldL1.children.filter(field => field.code == secondLevel).length == 0)) {
  //                   console.log("Level 2 not found for "+rows[i]);
  //                 }
  //                 break;
  //               }
  //             }
  //
  //             if(!added && fos.filter(field => field.code == firstLevel).length == 0) {
  //               console.log("Level 1 not found for |"+rows[i]+"|");
  //             }
  //           }
  //         }
  //
  //         for(let l1 of fos) {
  //           if(duplicates.has(l1.label)) {
  //             duplicates.get(l1.label).push(l1.id);
  //           } else {
  //             duplicates.set(l1.label, [l1.id]);
  //           }
  //           if(l1.children) {
  //             for (let l2 of l1.children) {
  //               if(duplicates.has(l2.label)) {
  //                 duplicates.get(l2.label).push(l2.id);
  //               } else {
  //                 duplicates.set(l2.label, [l2.id]);
  //               }
  //
  //               if(l2.children) {
  //                 for(let l3 of l2.children) {
  //                   if(duplicates.has(l3.label)) {
  //                     duplicates.get(l3.label).push(l3.id);
  //                   } else {
  //                     duplicates.set(l3.label, [l3.id]);
  //                   }
  //
  //                   if(l3.children) {
  //                     for(let l4 of l3.children) {
  //                       if(duplicates.has(l4.label)) {
  //                         duplicates.get(l4.label).push(l4.id);
  //                       } else {
  //                         duplicates.set(l4.label, [l4.id]);
  //                       }
  //                     }
  //                   }
  //                 }
  //               }
  //             }
  //           }
  //         }
  //
  //         for(let label of duplicates.keys()) {
  //           if(duplicates.get(label).length > 1) {
  //             console.log(label, duplicates.get(label));
  //           }
  //         }
  //         var url = window.URL.createObjectURL(new Blob([JSON.stringify(fos)]));
  //         var a = window.document.createElement('a');
  //         window.document.body.appendChild(a);
  //         a.setAttribute('style', 'display: none');
  //         a.href = url;
  //         a.download = "fosNewL4.json";
  //         a.click();
  //         window.URL.revokeObjectURL(url);
  //         a.remove(); // remove the element
  //
  //         // var theJSON = JSON.stringify(fos);
  //         // var uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
  //         // this.downloadJsonHref = uri;
  //         // console.log(fos);
  //       });
  //   });
  // }
  //
  // checkDifferences() {
  //   this.ISVocabulariesService.getIndexedFosL4(properties).subscribe(fosRes => {
  //     console.log(fosRes);
  //     this.ISVocabulariesService.getFos(properties).subscribe(currentFos => {
  //       console.log(currentFos);
  //       let currentSet: Map<string, string> = new Map<string, string>();
  //       for(let currentL1 of currentFos['fos']) {
  //         if(currentL1.children?.length > 0) {
  //           for(let currentL2 of currentL1.children) {
  //             if(currentL2.children?.length > 0) {
  //               for(let currentL3 of currentL2.children) {
  //                 if(currentL3.children?.length > 0) {
  //                   for(let currentL4 of currentL3.children) {
  //                     if(currentSet.has(currentL4.code)) {
  //                       console.log("ALREADY HERE ",currentL4.code);
  //                     }
  //                     currentSet.set(currentL4.code, currentL4.label);
  //                   }
  //                 }
  //               }
  //             }
  //           }
  //         }
  //       }
  //
  //       console.log(currentSet);
  //       for (let fos of fosRes) {
  //         if(!currentSet.has(fos.l4code)) {
  //           console.log(fos.l4code + " does not exist in current voc");
  //         } else {
  //           if(currentSet.get(fos.l4code) != fos.l4label) {
  //             console.log(fos.l4code + " different label");
  //           }
  //         }
  //       }
  //
  //       for(let code of currentSet.keys()) {
  //         let found = false;
  //         for(let fos of fosRes) {
  //           if(fos.l4code == code) {
  //             found = true;
  //             break;
  //           }
  //         }
  //         if(!found) {
  //           console.log("current code "+code+" not indexed");
  //         }
  //       }
  //     });
  //   });
  // }


  ngOnInit() {
    if (typeof document !== 'undefined') {
      this.isClient = true;
    }
    this.configurationService.initPortal(this.properties, this.properties.adminToolsCommunity);
    this.feedbackmail = this.properties.feedbackmail;
    if (this.properties.environment == "production" || this.properties.environment == "development") {
      this.subscriptions.push(this.route.queryParams.subscribe(data => {
        this._meta.updateTag({content: 'all', name: 'robots'});
        this.seoService.removeLinkForPrevURL();
        this.seoService.removeLinkForNextURL();
      }));
    }
    this.subscriptions.push(this.userManagementService.getUserInfo().subscribe(user => {
      this.user = user;
      this.buildMenu();
      this.header = {
        route: "/",
        url: null,
        title: 'explore',
        logoUrl: 'assets/common-assets/logo-services/explore/main.svg',
        logoSmallUrl: 'assets/common-assets/logo-services/explore/small.svg',
        position: 'left',
        badge: true
      };
			this.reset();
    }));
    this.subscriptions.push(this.layoutService.hasQuickContact.subscribe(hasQuickContact => {
      if(this.showQuickContact !== hasQuickContact) {
        this.showQuickContact = hasQuickContact;
        this.cdr.detectChanges();
      }
    }));
		this.subscriptions.push(this.quickContactService.isDisplayed.subscribe(display => {
      if(this.displayQuickContact !== display) {
        this.displayQuickContact = display;
        this.cdr.detectChanges();
      }
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      } else if (typeof IntersectionObserver !== "undefined" && subscription instanceof IntersectionObserver) {
        subscription.disconnect();
      }
    });
    this.configurationService.clearSubscriptions();
    this.userManagementService.clearSubscriptions();
    this.smoothScroll.clearSubscriptions();
  }

	ngAfterViewInit() {
		if (typeof window !== "undefined") {
      this.createObservers();
      this.subscriptions.push(this.layoutService.rootClass.subscribe(rootClass => {
        this.loading = true;
        this.cdr.detectChanges();
        if (rootClass && this.isClient) {
          let link = <HTMLLinkElement>document.getElementById('theme');
          let append = false;
          if (!link) {
            link = document.createElement('link');
            link.rel = 'stylesheet';
            link.id = 'theme';
            append = true;
          }
          link.href = rootClass + '.css';
          link.onerror = (error) => console.log(error);
          if (append) {
            document.head.appendChild(link);
          }
        }
        this.rootClass = rootClass;
        this.cdr.detectChanges();
        setTimeout(() => {
          this.loading = false;
          this.cdr.detectChanges();
        }, 500)
      }));
    }
	}

	createObservers() {
		let options = {
			root: null,
			rootMargin: '0px',
			threshold: 0.1
		};
		let intersectionObserver = new IntersectionObserver(entries => {
			entries.forEach(entry => {
        if(this.bottomNotIntersecting !== (!entry.isIntersecting)) {
          this.bottomNotIntersecting = !entry.isIntersecting;
          this.cdr.detectChanges();
        }
      });
		}, options);
		intersectionObserver.observe(this.bottom.nativeElement);
		this.subscriptions.push(intersectionObserver);
	}
  
  buildMenu() {
		this.userMenuItems = [];
		this.userMenuItems.push(new MenuItem("", "My profile", "", "", false, [], [], {}));
    if(this.properties.environment != "beta") {
      this.userMenuItems.push(new MenuItem("", "My ORCID links", "", "/my-orcid-links", false, [], [""], {}));
    }
    this.userMenuItems.push(new MenuItem("", "My links", "", "/myclaims", false, [], ["/myclaims"], {}));
    let researchOutcomesMenu = new MenuItem("", OpenaireEntities.RESULTS, "", "/search/find/research-outcomes", false, [], ["/search/find/research-outcomes"], {});
    researchOutcomesMenu.items = [
      new MenuItem("", OpenaireEntities.PUBLICATIONS, "", "/search/find/research-outcomes", false, [], ["/search/find/research-outcomes"], {type: '"' + encodeURIComponent("publications") + '"'}),
      new MenuItem("", OpenaireEntities.DATASETS, "", "/search/find/research-outcomes", false, [], ["/search/find/research-outcomes"], {type: '"' + encodeURIComponent("datasets") + '"'}),
      new MenuItem("", OpenaireEntities.SOFTWARE, "", "/search/find/research-outcomes", false, [], ["/search/find/research-outcomes"], {type: '"' + encodeURIComponent("software") + '"'}),
      new MenuItem("", OpenaireEntities.OTHER, "", "/search/find/research-outcomes", false, [], ["/search/find/research-outcomes"], {type: '"' + encodeURIComponent("other") + '"'})];
    //TODO add check for research results route
    this.menuItems = [
      new MenuItem("search", "Search", "", "/search/find/research-outcomes", false, [], ["/search/find/research-outcomes"], {},
        null, null, null, null, "_blank", "internal", false,
        [
          researchOutcomesMenu,
          new MenuItem("", OpenaireEntities.PROJECTS, "", "/search/find/projects/", false, ["project"], ["/search/find/projects"], {}),
          new MenuItem("", OpenaireEntities.DATASOURCES, "", "/search/find/dataproviders", false, ["datasource"], ["/search/find/dataproviders"], {}),
          new MenuItem("", OpenaireEntities.ORGANIZATIONS, "", "/search/find/organizations/", false, ["organization"], ["/search/find/organizations"], {})
        ]
      ),
      new MenuItem("deposit", "Deposit", "", "/participate/deposit/learn-how", false, [], ["/participate/deposit/learn-how"], {}),
      new MenuItem("link", "Link", "", "/participate/claim", false, [], ["/participate/claim"], {},
        null, null, null, null, "_blank", "internal", false,
        [new MenuItem("", "Start linking", "", "/participate/claim", false, [], ["/participate/claim"], {}),
          new MenuItem("", "Learn more", this.properties.claimsInformationLink, "", false, [], [], {})]
      ),
      new MenuItem("datasources", OpenaireEntities.DATASOURCES, "", "", false, ["datasource"], [], {},
        null, null, null, null, "_blank", "internal", false,
        [new MenuItem("", "Data Policies", "https://beta.openaire.eu/oa-policies-mandates", "", false, ["datasource"], [""], {}),
          new MenuItem("", "Repositories", "", "/search/content-providers", false, ["datasource"], ["/search/content-providers"], {}),
          new MenuItem("", "Journals", "", "/search/journals", false, ["datasource"], ["/search/journals"], {}),
          new MenuItem("", "Registries", "", "/search/entity-registries", false, ["datasource"], ["/search/entity-registries"], {}),
          new MenuItem("", "Browse all", "", "/search/find/dataproviders", false, ["datasource"], ["/search/find/dataproviders"], {})]
      ),
      new MenuItem("funders", "Funders", "", "/funders", false, [], ["/funders"], {}),
    ];
    if (Session.isPortalAdministrator(this.user)) {
      this.userMenuItems.push(new MenuItem("", "Manage all links", "", "/claims", false, [], ["/claims"], {}));
      this.userMenuItems.push(new MenuItem("", "Manage helptexts", this.properties.adminPortalURL + "/openaire/admin-tools/pages", "", true, [], [], {}));
    } else if (Session.isClaimsCurator(this.user)) {
      this.userMenuItems.push(new MenuItem("", "Manage all links", "", "/claims", false, [], ["/claims"], {}));
      
    }
    if (this.user) {
      this.userMenuItems.push(new MenuItem("", "User information", "", "/user-info", false, [], [], {}));
    }
  }

	public send(event) {
    if (event.valid === true) {
      this.sendMail(this.properties.admins);
    }
  }

	private sendMail(admins: string[]) {
    this.sending = true;
    this.subscriptions.push(this.emailService.contact(this.properties,
      Composer.composeEmailForExplore(this.contactForm.value, admins),
      this.contactForm.value.recaptcha).subscribe(
      res => {
        if (res) {
          this.sending = false;
          this.reset();
          this.modalOpen();
        } else {
          this.handleError('Email <b>sent failed!</b> Please try again.');
        }
      },
      error => {
        this.handleError('Email <b>sent failed!</b> Please try again.', error);
      }
    ));
  }

	public reset() {
    if (this.quickContact) {
      this.quickContact.close();
    }
    this.contactForm = this.fb.group({
      name: this.fb.control('', Validators.required),
      surname: this.fb.control('', Validators.required),
      email: this.fb.control('', [Validators.required, Validators.email]),
      affiliation: this.fb.control(''),
      message: this.fb.control('', Validators.required),
      recaptcha: this.fb.control('', Validators.required),
    });
  }

	public modalOpen() {
    this.modal.okButton = true;
    this.modal.alertTitle = 'Your request has been successfully submitted';
    this.modal.message = 'Our team will respond to your submission soon.';
    this.modal.cancelButton = false;
    this.modal.okButtonLeft = false;
    this.modal.okButtonText = 'OK';
    this.modal.open();
  }

	handleError(message: string, error = null) {
    if (error) {
      console.error(error);
    }
    this.sending = false;
    this.quickContact.close();
    NotificationHandler.rise(message, 'danger');
    this.contactForm.get('recaptcha').setValue('');
  }
}

