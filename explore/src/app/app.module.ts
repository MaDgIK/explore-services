import {APP_ID, NgModule} from '@angular/core';
import {SharedModule} from './openaireLibrary/shared/shared.module';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {NavigationBarModule} from './openaireLibrary/sharedComponents/navigationBar.module';
import {CookieLawModule} from './openaireLibrary/sharedComponents/cookie-law/cookie-law.module';
import {BottomModule} from './openaireLibrary/sharedComponents/bottom.module';
import {ErrorModule} from './openaireLibrary/error/error.module';
import {AppRoutingModule} from './app-routing.module';
import {HttpInterceptorService} from "./openaireLibrary/http-interceptor.service";
import {PageURLResolverModule} from "./openaireLibrary/utils/pageURLResolver.module";
import {Schema2jsonldModule} from "./openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {CacheInterceptorService} from "./openaireLibrary/cache-interceptor.service";
import {DEFAULT_TIMEOUT, TimeoutInterceptor} from "./openaireLibrary/timeout-interceptor.service";
import {ErrorInterceptorService} from "./openaireLibrary/error-interceptor.service";
import {QuickContactModule} from './openaireLibrary/sharedComponents/quick-contact/quick-contact.module';
import {AlertModalModule} from './openaireLibrary/utils/modal/alertModal.module';
import {LoadingModule} from "./openaireLibrary/utils/loading/loading.module";

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    HttpClientModule,
    ErrorModule,
    NavigationBarModule, BottomModule,
    CookieLawModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule, PageURLResolverModule,
    Schema2jsonldModule,
    QuickContactModule,
    AlertModalModule, LoadingModule
  ],
  declarations: [AppComponent],
  exports: [AppComponent],
  providers: [
    {provide: APP_ID, useValue: 'explore'},
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: CacheInterceptorService,
    //   multi: true
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorService,
      multi: true
    },
    [{provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true}],
    [{provide: DEFAULT_TIMEOUT, useValue: 3000}]
  ],
  bootstrap: [AppComponent]
})
//
export class AppModule {
}
