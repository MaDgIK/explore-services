import { NgModule } from '@angular/core';

import { SharedModule } from '../../openaireLibrary/shared/shared.module';
import { OpenaireDirectLinkingComponent } from './directLinking.component';
import{ DirectLinkingModule} from '../../openaireLibrary/claims/directLinking/directLinking.module';
import {DirectLinkingRoutingModule} from './directLinking-routing.module';

@NgModule({
  imports: [
    SharedModule, DirectLinkingModule, DirectLinkingRoutingModule
  ],
  providers:[],
  declarations: [
    OpenaireDirectLinkingComponent
  ], exports:[OpenaireDirectLinkingComponent]
})
export class LibDirectLinkingModule { }
