import { NgModule } from '@angular/core';

import { SharedModule } from '../../openaireLibrary/shared/shared.module';
import {OpenaireLinkingComponent} from './linkingGeneric.component';
import {LinkingRoutingModule} from './linking-routing.module';
import{   LinkingGenericModule} from '../../openaireLibrary/claims/linking/linkingGeneric.module';

@NgModule({
  imports: [
    SharedModule, LinkingRoutingModule, LinkingGenericModule
  ],
  providers:[],
  declarations: [
    OpenaireLinkingComponent
   ], exports:[
   OpenaireLinkingComponent ]
})
export class LibLinkingGenericModule { }
