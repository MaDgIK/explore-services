import {Component, OnInit} from '@angular/core';
import {properties} from "../../../environments/environment";


@Component({
  selector: 'openaire-my-claims',
  template: `
    <my-claims *ngIf="claimsInfoURL" [claimsInfoURL]="claimsInfoURL">
    </my-claims>
  `
  
})
export class OpenaireMyClaimsComponent implements OnInit {
  claimsInfoURL: string;

  constructor() {
  }
  
  public ngOnInit() {
    this.claimsInfoURL = properties.claimsInformationLink;
  }
}
