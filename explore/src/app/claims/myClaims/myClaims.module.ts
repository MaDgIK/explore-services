import { NgModule } from '@angular/core';

import { SharedModule } from '../../openaireLibrary/shared/shared.module';
import { OpenaireMyClaimsComponent } from './myClaims.component';
import { MyClaimsRoutingModule } from './myClaims-routing.module';
import{  MyClaimsModule} from '../../openaireLibrary/claims/myClaims/myClaims.module';

@NgModule({
  imports: [
    SharedModule,
    MyClaimsRoutingModule,
    MyClaimsModule

  ],
  providers:[],
  declarations: [
    OpenaireMyClaimsComponent
  ]
})
export class LibMyClaimsModule { }
