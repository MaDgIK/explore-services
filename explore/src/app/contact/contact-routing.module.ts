import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {ContactComponent} from './contact.component';
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: '', component: ContactComponent, canActivate: [IsRouteEnabled], canDeactivate: [PreviousRouteRecorder]}
        ])
    ]
})
export class ContactRoutingModule { }
