import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {ContactComponent} from './contact.component';
import {ContactRoutingModule} from "./contact-routing.module";
import {AlertModalModule} from "../openaireLibrary/utils/modal/alertModal.module";
import {HelperModule} from "../openaireLibrary/utils/helper/helper.module";
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {ContactUsModule} from "../openaireLibrary/contact-us/contact-us.module";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";


@NgModule({
  imports: [
    ContactRoutingModule, CommonModule, RouterModule,
    AlertModalModule, HelperModule,
    Schema2jsonldModule, SEOServiceModule, ContactUsModule, BreadcrumbsModule, LoadingModule
  ],
  declarations: [
    ContactComponent
  ],
  providers: [],
  exports: [
    ContactComponent
  ]
})

export class ContactModule { }
