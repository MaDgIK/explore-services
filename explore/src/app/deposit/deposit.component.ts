import {Component, OnDestroy, OnInit} from '@angular/core';
import {LayoutService} from "../openaireLibrary/dashboard/sharedComponents/sidebar/layout.service";

@Component({
  selector: 'openaire-deposit',
  template: `    
    <deposit-first-page></deposit-first-page>
    `
})

export class OpenaireDepositComponent implements OnInit, OnDestroy {

  constructor(private layoutService: LayoutService) {
  }

  public ngOnInit() {
    this.layoutService.setRootClass('deposit');
  }

  ngOnDestroy() {
    this.layoutService.setRootClass(null);
  }
}

