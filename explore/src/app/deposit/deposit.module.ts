import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import {OpenaireDepositComponent} from "./deposit.component";
import {DepositRoutingModule} from "./deposit-routing.module";
import {DepositFirstPageModule} from "../openaireLibrary/deposit/depositFirstPage.module";

import {ZenodoCommunitiesService} from "../openaireLibrary/connect/zenodoCommunities/zenodo-communities.service";
import {CommunityService} from "../openaireLibrary/connect/community/community.service";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    DepositRoutingModule, DepositFirstPageModule
  ],
  declarations: [
    OpenaireDepositComponent
  ],
  exports: [
    OpenaireDepositComponent,
  ],
  providers: [
    ZenodoCommunitiesService, CommunityService
  ]
})
export class LibDepositModule { }
