import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import { OpenaireSearchDataprovidersToDepositComponent } from './searchDataprovidersToDeposit.component';

import {SearchDataprovidersToDepositRoutingModule} from './searchDataprovidersToDeposit-routing.module';
import {SearchDataprovidersToDepositModule} from '../openaireLibrary/deposit/searchDataprovidersToDeposit.module';
import {ZenodoCommunitiesServiceModule} from '../openaireLibrary/connect/zenodoCommunities/zenodo-communitiesService.module';
import {CommunityService} from '../openaireLibrary/connect/community/community.service';

@NgModule({
  imports: [
    CommonModule, FormsModule,
    SearchDataprovidersToDepositRoutingModule,
    SearchDataprovidersToDepositModule,
    ZenodoCommunitiesServiceModule
  ],
  declarations: [
    OpenaireSearchDataprovidersToDepositComponent
  ],
  exports: [
    OpenaireSearchDataprovidersToDepositComponent,
  ],
  providers: [CommunityService]
})
export class LibSearchDataprovidersToDepositModule { }
