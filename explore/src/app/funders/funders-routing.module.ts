import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {FundersComponent} from './funders.component';
import {IsRouteEnabled} from "../openaireLibrary/error/isRouteEnabled.guard";
import {PreviousRouteRecorder} from "../openaireLibrary/utils/piwik/previousRouteRecorder.guard";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '', component: FundersComponent,
        canActivate: [IsRouteEnabled],
        canDeactivate: [PreviousRouteRecorder]
      }
    ])
  ]
})
export class FundersRoutingModule { }
