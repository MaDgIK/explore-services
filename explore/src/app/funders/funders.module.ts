import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {FundersComponent} from './funders.component';
import {FundersRoutingModule} from "./funders-routing.module";
import {AlertModalModule} from "../openaireLibrary/utils/modal/alertModal.module";
import {Schema2jsonldModule} from "../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module";
import {SEOServiceModule} from "../openaireLibrary/sharedComponents/SEO/SEOService.module";
import {BreadcrumbsModule} from "../openaireLibrary/utils/breadcrumbs/breadcrumbs.module";
import {LoadingModule} from "../openaireLibrary/utils/loading/loading.module";
import {RefineFieldResultsServiceModule} from '../openaireLibrary/services/refineFieldResultsService.module';
import {LogoUrlPipeModule} from '../openaireLibrary/utils/pipes/logoUrlPipe.module';
import {IconsModule} from '../openaireLibrary/utils/icons/icons.module';
import {IconsService} from '../openaireLibrary/utils/icons/icons.service';
import {open_access} from '../openaireLibrary/utils/icons/icons';
import {closed_access} from '../openaireLibrary/utils/icons/icons';
import {InputModule} from '../openaireLibrary/sharedComponents/input/input.module';
import {PagingModule} from '../openaireLibrary/utils/paging.module';
import {SearchInputModule} from '../openaireLibrary/sharedComponents/search-input/search-input.module';
import {GroupedRequestsServiceModule} from "../openaireLibrary/services/groupedRequestsService.module";


@NgModule({
  imports: [
    FundersRoutingModule, CommonModule, RouterModule, AlertModalModule,
    Schema2jsonldModule, SEOServiceModule, BreadcrumbsModule, LoadingModule,
    RefineFieldResultsServiceModule, LogoUrlPipeModule, IconsModule, InputModule,
    PagingModule, SearchInputModule, GroupedRequestsServiceModule
  ],
  declarations: [
    FundersComponent
  ],
  providers: [],
  exports: [
    FundersComponent
  ]
})

export class FundersModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([open_access, closed_access]);
  }
}
