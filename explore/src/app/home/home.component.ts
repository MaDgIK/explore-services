import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {Subscriber} from 'rxjs';
import {Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {ConfigurationService} from '../openaireLibrary/utils/configuration/configuration.service';
import {OpenaireEntities, SearchFields} from '../openaireLibrary/utils/properties/searchFields';
import {RouterHelper} from '../openaireLibrary/utils/routerHelper.class';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {ErrorCodes} from '../openaireLibrary/utils/properties/errorCodes';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';
import {HelperService} from "../openaireLibrary/utils/helper/helper.service";
import {Filter} from "../openaireLibrary/searchPages/searchUtils/searchHelperClasses.class";
import {properties} from "../../environments/environment";
import {Numbers, NumbersComponent} from "../openaireLibrary/sharedComponents/numbers/numbers.component";
import {StringUtils} from "../openaireLibrary/utils/string-utils.class";
import {QuickContactService} from '../openaireLibrary/sharedComponents/quick-contact/quick-contact.service';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.less']
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {
  // MAX 12 logos in every slide
  public logos = {
    "publication": [
      [
        "publications.png"
      ],
      [
        "publications2.png"
      ]
    ],
    "software": [
      [
        "software.png"
      ]
    ],
    "dataset": [
      [
        "datasets.png"
      ]
    ],
    "persistent": [
      [
        "pids.png"
      ]
    ],
    "funder": [
      [
        "funders.png"
      ]
    ]
  };
  public portalName: string = "";
  public pageTitle = "OpenAIRE";
  public keyword: string = "";
  public searchFields: SearchFields = new SearchFields();
  public errorCodes: ErrorCodes = new ErrorCodes();
  public routerHelper: RouterHelper = new RouterHelper();
  public numbers: Numbers = {};
  public properties: EnvProperties = properties;
  public openaireEntities = OpenaireEntities;
  public readMore: boolean = false;
	@ViewChild('contact') contact: ElementRef;
  subscriptions: any[] = [];
  @ViewChildren('scrolling_element') elements: QueryList<ElementRef>;
  resultsQuickFilter: { filter: Filter, selected: boolean, filterId: string, value: string } = null;//{
    // filter: null,
    // selected: true,
    // filterId: "resultbestaccessright",
    // value: "Open Access"
  // };
  selectedEntity = "all";
  selectedEntitySimpleUrl;
  selectedEntityAdvancedUrl;
  resultTypes: Filter = {
    values: [],
    filterId: "type",
    countSelectedValues: 0,
    filterType: 'checkbox',
    originalFilterId: "",
    valueIsExact: true,
    title: "Type",
    filterOperator: "or"
  };
  public pageContents = null;
  @ViewChild("numbersComponent", {static: true}) numbersComponent: NumbersComponent;
  specialSubjects = [
    {value: "Physics::Atomic Physics", label: "Atomic Physics"},
    {value: "Mathematics::Combinatorics", label: "Mathematics Combinatorics"},
    {value: "lcsh:Medicine", label: "Medicine"},
    {value: "lcsh:Science", label: "Science"},
    {value: "lcsh:Biology (General)", label: "Biology"},
    {value: "lcsh:Chemistry", label: "Chemistry"},
    {value: "lcsh:Engineering (General). Civil engineering (General)", label: "Civil engineering"},
    {value: "lcsh:Technology", label: "Technology"},
  ];
  stringUtils = new StringUtils();
  // disableSelect: boolean = true;
  
  constructor(
    private _router: Router,
    private _piwikService: PiwikService,
    private config: ConfigurationService, private _meta: Meta, private _title: Title, private seoService: SEOService,
    private helper: HelperService,
    private cdr: ChangeDetectorRef,
		private quickContactService: QuickContactService
  ) {
    let description = "OpenAIRE Explore: Over 100M of research deduplicated, 170K research software, 11M research data. One of the largest open scholarly records collection worldwide.";
    let title = "OpenAIRE | Find and Share research";
    this._title.setTitle(title);
    this._meta.updateTag({content: description}, "name='description'");
    this._meta.updateTag({content: description}, "property='og:description'");
    this._meta.updateTag({content: title}, "property='og:title'");
    this.quickContactService.setDisplay(false);
  }
  
  private getPageContents() {
    this.subscriptions.push(this.helper.getPageHelpContents(this.properties, 'openaire', this._router.url).subscribe(contents => {
      this.pageContents = contents;
    }));
  }
  
  public getKeys(obj: {}) {
    return Object.keys(obj);
  }
  
  createRange(number) {
    var items: number[] = [];
    for (var i = 1; i <= number; i++) {
      items.push(i);
    }
    return items;
  }
  
  public ceil(num: number) {
    return Math.ceil(num);
  }
  
  clear() {
    this.subscriptions.forEach(value => {
      if (value instanceof Subscriber) {
        value.unsubscribe();
      } else if (value instanceof IntersectionObserver || value instanceof MutationObserver) {
        value.disconnect();
      }
    });
  }
  
  public ngOnInit() {
    this.seoService.createLinkForCanonicalURL(this.properties.domain + this.properties.baseLink + this._router.url, false);
    this.getPageContents();
    if (this.properties != null) {
      var url = this.properties.domain + this.properties.baseLink + this._router.url;
      this._meta.updateTag({content: url}, "property='og:url'");
      this.subscriptions.push(this._piwikService.trackView(this.properties, "OpenAIRE").subscribe());
			if(this.numbersComponent) {
				this.numbersComponent.showPublications = false;
				this.numbersComponent.showDatasets = false;
				this.numbersComponent.showSoftware = false;
				this.numbersComponent.showOrp = false;
				this.numbersComponent.showProjects = false;
				this.numbersComponent.showDataProviders = false;
				this.numbersComponent.showOrganizations = false;
			}
      this.subscriptions.push(this.config.portalAsObservable.subscribe(data => {
          if(data) {
            if(data.name) {
              this.portalName = data.name;
            }
            var showEntity = {};
            for (var i = 0; i < data['entities'].length; i++) {
              showEntity["" + data['entities'][i]["pid"] + ""] = data['entities'][i]["isEnabled"];
            }
						if(this.numbersComponent) {
							this.numbersComponent.showPublications = !!showEntity["publication"];
							this.numbersComponent.showDatasets = !!showEntity["dataset"];
							this.numbersComponent.showSoftware = !!showEntity["software"];
							this.numbersComponent.showOrp = !!showEntity["orp"];
							this.numbersComponent.showProjects = !!showEntity["project"];
							this.numbersComponent.showDataProviders = !!showEntity["datasource"];
							this.numbersComponent.showOrganizations = !!showEntity["organization"];
						}
            if(this.numbersComponent && this.numbersComponent.showPublications) {
              this.resultTypes.values.push({
                name: this.openaireEntities.PUBLICATIONS,
                id: "publications",
                selected: false,
                number: 0
              });
            }
            if(this.numbersComponent && this.numbersComponent.showDatasets) {
              this.resultTypes.values.push({
                name: this.openaireEntities.DATASETS,
                id: "datasets",
                selected: false,
                number: 0
              });
            }
            if(this.numbersComponent && this.numbersComponent.showSoftware) {
              this.resultTypes.values.push({
                name: this.openaireEntities.SOFTWARE,
                id: "software",
                selected: false,
                number: 0
              });
            }
            if(this.numbersComponent && this.numbersComponent.showOrp) {
              this.resultTypes.values.push({name: this.openaireEntities.OTHER, id: "other", selected: false, number: 0});
            }
						if(this.numbersComponent) {
							this.numbersComponent.init(this.numbersComponent.showDatasets, this.numbersComponent.showSoftware, this.numbersComponent.showPublications, this.numbersComponent.showDatasets, this.numbersComponent.showSoftware, this.numbersComponent.showOrp, this.numbersComponent.showProjects, this.numbersComponent.showDataProviders, this.numbersComponent.showOrganizations);
						}
          }
        },
        error => {
          this.handleError("Error getting community information", error);
        }
      ));
    }
  }
  
  
  public ngOnDestroy() {
    this.quickContactService.setDisplay(true);
    this.clear();
  }
  
  ngAfterViewInit() {
    if (typeof window !== "undefined") {
      this.createObservers();
    }
  }
  
  createObservers() {
		let options = {
      root: null,
      rootMargin: '200px',
      threshold: 1.0
    };
    let intersectionObserver = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        this.quickContactService.setDisplay(!entry.isIntersecting);
      });
    }, options);
		intersectionObserver.observe(this.contact.nativeElement);
    let mutationObserver = new MutationObserver(entries => {
      entries.forEach(entry => {
        if (entry.attributeName === 'style') {
          let opacities: number[] = this.elements.map(element => +element.nativeElement.style.opacity);
          let active: number = opacities.indexOf(Math.max(...opacities));
          this.elements.forEach((element, index) => {
            if (index === active) {
              element.nativeElement.classList.remove('uk-disabled');
            } else {
              element.nativeElement.classList.add('uk-disabled');
            }
          })
        }
      })
    });
    this.elements.forEach(element => {
      mutationObserver.observe(element.nativeElement, {attributes: true});
    });
		this.subscriptions.push(intersectionObserver);
    this.subscriptions.push(mutationObserver);
  }
  
  private handleError(message: string, error) {
    console.error("Home Page: " + message, error);
  }
  
  entityChanged($event) {
    this.selectedEntity = $event.entity;
    this.selectedEntitySimpleUrl = $event.simpleUrl;
    this.selectedEntityAdvancedUrl = $event.advancedUrl;
  }
  
  goTo() {
    let parameterNames = [];
    let parameterValues = [];
    // if (this.selectedEntity == "result") {
    //   if (this.resultTypes) {
    //     let values = [];
    //     for (let value of this.resultTypes.values) {
    //       if (value.selected) {
    //         values.push(value.id);
    //       }
    //     }
    //     if (values.length > 0 && values.length != 4) {
    //       parameterNames.push("type");
    //       parameterValues.push(values.join(","));
    //     }
    //     if (this.resultsQuickFilter && this.resultsQuickFilter.selected) {
    //       parameterNames.push(this.resultsQuickFilter.filterId);
    //       parameterValues.push('"' + encodeURIComponent(this.resultsQuickFilter.value) + '"');
    //     }
    //   }
    // } else if (this.selectedEntity == "all") {
    if (this.resultsQuickFilter && this.resultsQuickFilter.selected) {
      parameterNames.push(this.resultsQuickFilter.filterId);
      parameterValues.push('"' + encodeURIComponent(this.resultsQuickFilter.value) + '"');
    }
    // }
    if (this.keyword.length > 0) {
      parameterNames.push("fv0");
      parameterValues.push(this.keyword);
      parameterNames.push("f0");
      parameterValues.push("q");
    }
    this._router.navigate([this.properties.searchLinkToAll], {queryParams: this.routerHelper.createQueryParams(parameterNames, parameterValues)});
  }
  
  getSubjectParameter(param) {
    return {'f0': 'resultsubject', 'fv0': '"' + (param) + '"', size: 50};
  }
  
  // disableSelectChange(event: boolean) {
  //   this.disableSelect = event;
  //   this.cdr.detectChanges();
  // }
}
