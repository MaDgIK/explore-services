import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';
import { RouterModule } from '@angular/router';

import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';

import { DataProvidersServiceModule} from '../openaireLibrary/services/dataProvidersService.module';
import { SearchResearchResultsServiceModule} from '../openaireLibrary/services/searchResearchResultsService.module';
import { ProjectsServiceModule} from '../openaireLibrary/services/projectsService.module';
import { OrganizationsServiceModule} from '../openaireLibrary/services/organizationsService.module';

import {HelperModule} from '../openaireLibrary/utils/helper/helper.module';

import {RefineFieldResultsServiceModule} from '../openaireLibrary/services/refineFieldResultsService.module';
import { SEOServiceModule } from '../openaireLibrary/sharedComponents/SEO/SEOService.module';

import {OtherPortalsModule} from "../openaireLibrary/sharedComponents/other-portals/other-portals.module";
import {EntitiesSelectionModule} from "../openaireLibrary/searchPages/searchUtils/entitiesSelection.module";
import {QuickSelectionsModule} from "../openaireLibrary/searchPages/searchUtils/quick-selections.module";
import {IconsModule} from "../openaireLibrary/utils/icons/icons.module";
import {IconsService} from "../openaireLibrary/utils/icons/icons.service";
import {book, cog, database, earth} from "../openaireLibrary/utils/icons/icons";
import {NumbersModule} from "../openaireLibrary/sharedComponents/numbers/numbers.module";
import {AdvancedSearchInputModule} from "../openaireLibrary/sharedComponents/advanced-search-input/advanced-search-input.module";
import {InputModule} from "../openaireLibrary/sharedComponents/input/input.module";
import {SearchInputModule} from "../openaireLibrary/sharedComponents/search-input/search-input.module";
import {SliderUtilsModule} from "../openaireLibrary/sharedComponents/slider-utils/slider-utils.module";

 @NgModule({
   imports: [
     HomeRoutingModule,
     CommonModule, FormsModule, RouterModule,
     RefineFieldResultsServiceModule,
     DataProvidersServiceModule, SearchResearchResultsServiceModule,
     ProjectsServiceModule, OrganizationsServiceModule,
     HelperModule,
     SEOServiceModule, OtherPortalsModule, EntitiesSelectionModule, QuickSelectionsModule, IconsModule, NumbersModule, AdvancedSearchInputModule, InputModule, SearchInputModule, SliderUtilsModule
   ],
  declarations: [
    HomeComponent
   ],
  providers:[],
  exports: [
    HomeComponent
     ]
})
export class HomeModule {
   constructor(private iconsService: IconsService) {
     this.iconsService.registerIcons([book, cog, database, earth]);
   }
 }
