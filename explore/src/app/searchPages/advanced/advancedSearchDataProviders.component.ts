import {Component} from '@angular/core';


@Component({
    selector: 'openaire-advanced-search-dataprovider',
    template: `
    <search-dataproviders  [simpleView]="false">
    </search-dataproviders>

    `
 })

export class OpenaireAdvancedSearchDataProvidersComponent { 
}
