import {Component} from '@angular/core';


@Component({
    selector: 'openaire-advanced-search-organizations',
    template: `
    <search-organizations [simpleView]="false">
    </search-organizations>

    `
 })

export class OpenaireAdvancedSearchOrganizationsComponent {


}
