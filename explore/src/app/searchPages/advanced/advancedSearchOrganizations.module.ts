import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {AdvancedSearchOrganizationsRoutingModule} from './advancedSearchOrganizations-routing.module';
import {OpenaireAdvancedSearchOrganizationsComponent} from './advancedSearchOrganizations.component';

import {SearchOrganizationsModule} from "../../openaireLibrary/searchPages/searchOrganizations.module";

@NgModule({
  imports: [
    AdvancedSearchOrganizationsRoutingModule,
    CommonModule, FormsModule,
    SearchOrganizationsModule
  ],
  declarations: [
    OpenaireAdvancedSearchOrganizationsComponent
   ],
  providers:[],
  exports: [
    OpenaireAdvancedSearchOrganizationsComponent
     ]
})
export class LibAdvancedSearchOrganizationsModule { }
