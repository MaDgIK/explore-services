import {Component} from '@angular/core';
@Component({
    selector: 'openaire-advanced-search-projects',
    template: `
      <search-projects [simpleView]="false">
      </search-projects>

    `
 })

export class OpenaireAdvancedSearchProjectsComponent { 

}
