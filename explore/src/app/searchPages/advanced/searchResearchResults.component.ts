import {Component, Input} from '@angular/core';

@Component({
    selector: 'openaire-search-results',
    template: `
      <search-research-results resultType="result" [simpleView]="false"></search-research-results>
    `

})
export class OpenaireSearchResearchResultsComponent {
    @Input() searchLink: string = "/search/advanced/research-results";

}
