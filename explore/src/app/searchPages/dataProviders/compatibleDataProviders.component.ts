import {Component} from '@angular/core';

@Component({
    selector: 'openaire-search-content-providers',
    template: `
      
    <search-dataproviders  [simpleView]="true" type="compatible" simpleSearchLink="/search/content-providers"  tableViewLink="/search/content-providers-table">
    </search-dataproviders>

    `

})
export class OpenaireSearchCompatibleDataprovidersComponent {

}
