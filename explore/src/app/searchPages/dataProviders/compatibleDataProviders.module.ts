import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import{ CompatibleDataProvidersRoutingModule} from './compatibleDataProviders-routing.module';
import{OpenaireSearchCompatibleDataprovidersComponent} from './compatibleDataProviders.component';

import {SearchDataProvidersModule} from "../../openaireLibrary/searchPages/searchDataProviders.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    CompatibleDataProvidersRoutingModule, SearchDataProvidersModule

  ],
  declarations: [
    OpenaireSearchCompatibleDataprovidersComponent
   ],
  providers:[],
  exports: [
    OpenaireSearchCompatibleDataprovidersComponent
     ]
})
export class LibCompatibleDataProvidersModule { }
