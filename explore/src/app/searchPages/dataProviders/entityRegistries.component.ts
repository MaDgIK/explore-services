import {Component} from '@angular/core';

@Component({
    selector: 'openaire-search-entity-registries',
    template: `

    <search-dataproviders  [simpleView]="true" type="registries" simpleSearchLink="/search/entity-registries"  tableViewLink="/search/entity-registries-table">
    </search-dataproviders>
    `

})
export class OpenaireSearchEntityRegistriesComponent {

}
