import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import{ EntityRegistriesRoutingModule} from './entityRegistries-routing.module';
import{OpenaireSearchEntityRegistriesComponent} from './entityRegistries.component';

import {SearchDataProvidersModule} from "../../openaireLibrary/searchPages/searchDataProviders.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    EntityRegistriesRoutingModule, SearchDataProvidersModule

  ],
  declarations: [
    OpenaireSearchEntityRegistriesComponent
   ],
  providers:[],
  exports: [
    OpenaireSearchEntityRegistriesComponent
     ]
})
export class LibEntityRegistriesModule { }
