import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import{OpenaireSearchJournalsComponent} from './journals.component';
import {PreviousRouteRecorder} from '../../openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {properties} from "../../../environments/environment";


@NgModule({
  imports: [
    RouterModule.forChild([
     	{ path: '', component: OpenaireSearchJournalsComponent, data: {
          redirect: properties.errorLink,  community : 'openaire'
        }, canDeactivate: [PreviousRouteRecorder]  }

    ])
  ]
})
export class JournalsRoutingModule { }
