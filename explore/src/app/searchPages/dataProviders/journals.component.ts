import {Component} from '@angular/core';


@Component({
    selector: 'openaire-search-journals',
    template: `      
    <search-dataproviders  [simpleView]="true" type="journals" simpleSearchLink="/search/journals" >
    </search-dataproviders>
    `

})
export class OpenaireSearchJournalsComponent {
}
