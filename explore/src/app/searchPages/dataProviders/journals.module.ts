import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import{ JournalsRoutingModule} from './journals-routing.module';
import{OpenaireSearchJournalsComponent} from './journals.component';
import {SearchDataProvidersModule} from "../../openaireLibrary/searchPages/searchDataProviders.module";
@NgModule({
  imports: [
    CommonModule, FormsModule, JournalsRoutingModule, SearchDataProvidersModule

  ],
  declarations: [
    OpenaireSearchJournalsComponent
   ],
  providers:[],
  exports: [
    OpenaireSearchJournalsComponent
     ]
})
export class LibJournalsModule { }
