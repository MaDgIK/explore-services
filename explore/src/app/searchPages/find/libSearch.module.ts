import { NgModule}            from '@angular/core';
import { OpenaireSearchComponent } from './search.component';
import { MainSearchRoutingModule } from './mainSearch-routing.module';
import {SearchAllModule} from "../../openaireLibrary/searchPages/find/searchAll.module";

@NgModule({
  imports: [MainSearchRoutingModule,  SearchAllModule],
  declarations:[OpenaireSearchComponent],
  exports:[OpenaireSearchComponent]
})
export class LibMainSearchModule { }
