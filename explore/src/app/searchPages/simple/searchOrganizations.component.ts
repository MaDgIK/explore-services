import {Component} from '@angular/core';

@Component({
    selector: 'openaire-search-organizations',
    template: `

    <search-organizations>
    </search-organizations>

    `

})
export class OpenaireSearchOrganizationsComponent {

}
