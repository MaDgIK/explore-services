import {Component, Input} from '@angular/core';

@Component({
    selector: 'openaire-search-results',
    template: `
      <search-research-results resultType="result" [stickyForm]="false"></search-research-results>
    `

})
export class OpenaireSearchResearchResultsComponent {
  @Input() searchLink: string = "/search/research-results";

}
