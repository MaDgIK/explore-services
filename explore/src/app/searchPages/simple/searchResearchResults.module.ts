import { NgModule}            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FormsModule }         from '@angular/forms';

import{ SearchResearchResultsRoutingModule} from './searchResearchResults-routing.module';
import{OpenaireSearchResearchResultsComponent} from './searchResearchResults.component';

import {SearchResearchResultsModule} from "../../openaireLibrary/searchPages/searchResearchResults.module";

@NgModule({
  imports: [
    CommonModule, FormsModule,
    SearchResearchResultsRoutingModule, SearchResearchResultsModule

  ],
  declarations: [
    OpenaireSearchResearchResultsComponent
   ],
  providers:[],
  exports: [
    OpenaireSearchResearchResultsComponent
     ]
})
export class OpenaireSearchResearchResultsModule { }
