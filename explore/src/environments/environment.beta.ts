import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonBeta} from "../app/openaireLibrary/utils/properties/environments/environment";
let props: EnvProperties = {
  enablePiwikTrack: true,
  piwikSiteId: "6",
  useCache: false,
  useLongCache: true,
  showAddThis: true,
  domain: "https://beta.explore.openaire.eu",
  adminToolsPortalType: "explore",
  dashboard: "explore",
  adminToolsCommunity: "openaire",
  baseLink : ""

};

export let properties: EnvProperties = {
    ...common, ...commonBeta,...props

}
