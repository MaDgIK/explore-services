import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonProd} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {
   enablePiwikTrack: true,
   piwikSiteId: "109",
   useCache: false,
   useLongCache: true,
   showAddThis: true,
   domain: "https://explore.openaire.eu",
  adminToolsPortalType: "explore",
  dashboard: "explore",
  adminToolsCommunity: "openaire",
  baseLink : ""
};

export let properties: EnvProperties = {
 ...common, ...commonProd, ...props
}
