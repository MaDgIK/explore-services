import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonTest} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {

  enablePiwikTrack: false,
  useCache: false,
  useLongCache: false,
  showAddThis: true,
  domain: "https://test.explore.openaire.eu",
  adminToolsPortalType: "explore",
  dashboard: "explore",
  adminToolsCommunity: "openaire",
  baseLink : "",
  errorLink: '/error',
  myOrcidLinksPage: "/my-orcid-links",
  footerGrantText: "OpenAIRE has received funding from <a href='https://www.openaire.eu/projects' target='_blank'>a series of EU funded projects</a>.",
  piwikSiteId: "109"

};

export let properties: EnvProperties = {
  ...common, ...commonTest, ...props
}
