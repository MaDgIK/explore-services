// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import {EnvProperties} from "../app/openaireLibrary/utils/properties/env-properties";
import {common, commonDev} from "../app/openaireLibrary/utils/properties/environments/environment";

let props: EnvProperties = {

  enablePiwikTrack: false,
  piwikSiteId: "109",
  useCache: false,
  useLongCache: false,
  showAddThis: true,
  adminToolsPortalType: "explore",
  dashboard: "explore",
  adminToolsCommunity: "openaire",
  baseLink : "",
  domain: "https://explore.openaire.eu"


  /*environment: "development",
  adminToolsPortalType: "explore",
  dashboard: "explore",
  enablePiwikTrack: false,
  useCache: false,
  useLongCache: false,
  showAddThis: true,
  statisticsAPIURL: "http://vatopedi.di.uoa.gr:8080/stats/",
  statisticsFrameAPIURL: "https://beta.openaire.eu/stats/",
  statisticsFrameNewAPIURL: "https://stats.madgik.di.uoa.gr/stats-api/",
  bipFrameAPIURL: "https://bip.imsi.athenarc.gr/api/impact-chart?id=",
  useNewStatistisTool: true,
  claimsAPIURL: "http://rudie.di.uoa.gr:8980/dnet-claims-service-2.0.0-SNAPSHOT/rest/claimsService/",
  // claimsAPIURL: "http://dl170.madgik.di.uoa.gr:8180/dnet-claims-service-2.0.0-SNAPSHOT/rest/claimsService/",
  searchAPIURLLAst: "http://beta.services.openaire.eu/search/v2/api/",
  searchResourcesAPIURL: "https://beta.services.openaire.eu/search/v2/api/resources",
  openCitationsAPIURL: "https://services.openaire.eu/opencitations/getCitations?id=",
  csvAPIURL: "https://beta.services.openaire.eu/search/v2/api/reports",
  searchCrossrefAPIURL: "https://api.crossref.org/works",
  searchDataciteAPIURL: "https://api.datacite.org/works",
  searchOrcidURL: "https://pub.orcid.org/v2.1/",
  orcidURL: "https://orcid.org/",
  orcidAPIURL: "http://duffy.di.uoa.gr:19480/uoa-orcid-service/",
  orcidTokenURL : "https://sandbox.orcid.org/oauth/authorize?",
  orcidClientId: "APP-A5M3KTX6NCN67L91",
  doiURL: "https://doi.org/",
  pmcURL: "http://europepmc.org/articles/",
  pmidURL: "https://www.ncbi.nlm.nih.gov/pubmed/",
  handleURL: "http://hdl.handle.net/",
  cordisURL: "http://cordis.europa.eu/projects/",
  openDoarURL: "http://v2.sherpa.ac.uk/id/repository/",
  r3DataURL: "http://service.re3data.org/repository/",
  swhURL: "https://archive.softwareheritage.org/",
  fairSharingURL: "https://fairsharing.org/",
  eoscMarketplaceURL: "https://marketplace.eosc-portal.eu/services/",
  sherpaURL: "http://sherpa.ac.uk/romeo/issn/",
  sherpaURLSuffix: "/",
  zenodo: "https://zenodo.org/",
  helpdesk: "https://www.openaire.eu/support/helpdesk",
  helpdeskEmail: "helpdesk@openaire.eu",
  utilsService: "http://dl170.madgik.di.uoa.gr:8000",

  vocabulariesAPI: "https://dev-openaire.d4science.org/provision/mvc/vocabularies/",

  piwikBaseUrl: "https://analytics.openaire.eu/piwik.php?idsite=",
  piwikSiteId: "6",
  cookieDomain: ".di.uoa.gr",

  feedbackmail: "kostis30fylloy@gmail.com",

  cacheUrl: "http://dl170.madgik.di.uoa.gr:3000/get?url=",

  monitorServiceAPIURL: "http://duffy.di.uoa.gr:19380/uoa-monitor-service",
  adminToolsAPIURL: "http://duffy.di.uoa.gr:19280/uoa-admin-tools/",

  // adminToolsCommunity: "openaire",
  datasourcesAPI: "https://beta.services.openaire.eu/openaire/ds/api/",
  contextsAPI: "https://dev-openaire.d4science.org/openaire/context",
  communityAPI: "https://dev-openaire.d4science.org/openaire/community/",

  csvLimit: 2000,
  pagingLimit: 20,
  resultsPerPage: 10,

  "baseLink" : "",
  "domain": "https://beta.explore.openaire.eu",

  searchLinkToResult: "/search/result?id=",
  searchLinkToPublication: "/search/publication?articleId=",
  searchLinkToProject: "/search/project?projectId=",
  searchLinkToDataProvider: "/search/dataprovider?datasourceId=",
  searchLinkToDataset: "/search/dataset?datasetId=",
  searchLinkToSoftwareLanding: "/search/software?softwareId=",
  searchLinkToOrp: "/search/other?orpId=",
  searchLinkToOrganization: "/search/organization?organizationId=",

  searchLinkToAll: "/search/find/",
  searchLinkToPublications: "/search/find/publications",
  searchLinkToDataProviders: "/search/find/dataproviders",
  searchLinkToProjects: "/search/find/projects",
  searchLinkToDatasets: "/search/find/datasets",
  searchLinkToSoftware: "/search/find/software",
  searchLinkToOrps: "/search/find/other",
  searchLinkToOrganizations: "/search/find/organizations",
  searchLinkToCompatibleDataProviders: "/search/content-providers",
  searchLinkToEntityRegistriesDataProviders: "/search/entity-registries",
  searchLinkToJournals: "/search/journals",
  searchLinkToResults: "/search/find/research-outcomes",

  searchLinkToAdvancedPublications: "/search/advanced/publications",
  searchLinkToAdvancedProjects: "/search/advanced/projects",
  searchLinkToAdvancedDatasets: "/search/advanced/datasets",
  searchLinkToAdvancedSoftware: "/search/advanced/software",
  searchLinkToAdvancedOrps: "/search/advanced/other",
  searchLinkToAdvancedDataProviders: "/search/advanced/dataproviders",
  searchLinkToAdvancedOrganizations: "/search/advanced/organizations",
  searchLinkToAdvancedResults: "/search/advanced/research-outcomes",
  errorLink: '/error',
  lastIndexInformationLink: "https://www.openaire.eu/aggregation-and-content-provision-workflows",
  showLastIndexInformationLink: true,
  widgetLink: "https://beta.openaire.eu/index.php?option=com_openaire&view=widget&format=raw&projectId=",
  claimsInformationLink: "https://www.openaire.eu/linking-beta",
  lastIndexUpdate: "2019-11-01",
  indexInfoAPI: "https://beta.services.openaire.eu/openaire/info/",

  depositLearnHowPage: "/participate/deposit/learn-how",
  depositSearchPage: "/participate/deposit/search",
  altMetricsAPIURL: "https://api.altmetric.com/v1/doi/",
  reCaptchaSiteKey: "6LcVtFIUAAAAAB2ac6xYivHxYXKoUvYRPi-6_rLu",
  admins: ['kostis30fylloy@gmail.com', 'alexandros.martzios@athenarc.gr', 'kgalouni@di.uoa.gr'],
  b2noteAPIURL: 'https://b2note.eudat.eu/',
  impactFactorsAPIURL: "https://bip-api.imsi.athenarc.gr/paper/scores/batch/",
  adminPortalURL: "https://beta.admin.connect.openaire.eu",

  myOrcidLinksPage: "/my-orcid-links",
  footerGrantText: "OpenAIRE has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541 and 101017452",

  egiNotebookLink: "https://marketplace.eosc-portal.eu/services/egi-notebooks?q=EGI+Notebook"*/

};

export let properties: EnvProperties = {
  ...common, ...commonDev, ...props
}
