export const ROUTES = [
  '/',
  '/lazy',
  '/home',
  '/search/publication','/search/project','/search/dataset','/search/dataprovider','/search/organization',
  '/search/find',
  '/search/person','/search/publication','/search/project','/search/dataset','/search/dataprovider','/search/organization',
  '/search/find/people','/search/find/publications','/search/find/projects','/search/find/datasets','/search/find/dataproviders','/search/find/organizations',
  '/search/advanced/people','/search/advanced/publications','/search/advanced/projects','/search/advanced/datasets','/search/advanced/dataproviders','/search/advanced/organizations',
  '/participate/deposit-publications','/participate/deposit-datasets','/participate/deposit-publications-result','/participate/deposit-datasets-result','/participate/deposit-subject-result',
  '/search/content-providers','/search/content-providers-table','/search/entity-registries','/search/entity-registries-table','/search/journals','/search/journals-table',
  '/project-report','/claims','/myclaims','/participate/claim','/participate/direct-claim','/claims-project-manager',
  '/test','/user-info',
  '/error', '/*path'
];
