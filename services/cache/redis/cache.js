const redis = require('redis');
const express = require('express');
const request = require('superagent');
const PORT = process.env.PORT;
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;

const app = express();
const client = redis.createClient(REDIS_PORT, REDIS_HOST);

// function respond(org, numberOfRepos) {
//     return `Organization "${org}" has ${numberOfRepos} public repositories.`;
// }

function cache(req, res, next) {
    const url = req.query.url;
    client.get(url, function (err, data) {
        if (data != null) {
            res.send(data);
        } else {
            next();
        }
    });
}

// function getNumberOfRepos(req, res, next) {
//     const org = req.query.org;
//     request.get(`https://api.github.com/orgs/${org}/repos`, function (err, response) {
//         if (err) {
//             throw err;
//         }
//
//         var repoNumber = 0;
//         if (response && response.body) {
//             repoNumber = response.body.length;
//         }
//         client.setex(org, 5, repoNumber);
//         res.send(respond(org, repoNumber));
//     });
// }
//
// app.get('/repos', cache, getNumberOfRepos);
app.get('/get',cache, (req, res) => {
  setTimeout(() => {
    const url = (req.query)?req.query.url:null;
    if (!url){
        res.status(404).send('Not Found'); //not found

    }else{
     request.get(url, function (err, response) {
          if (err) throw err;

          // response.body contains an array of public repositories
          // var repoNumber = response.body.length;
          res.send(response.body);
    })
  }
})
});
app.listen(PORT, function () {
    console.log('app listening on port', PORT);
});
