# README #

 
### What is this repository for? ###

Utils service 

* Version 1.0.0
 
### How to run locally? ###

* "npm start" to start the service

### How to run in a server? ###
* run "npm run prepare-{CONFIGURATION_NAME}"
* cp dist folder in the server
* create a folder for the service files
* "npm i" to install dependencies. 
* if you just update the project run "pm2 restart upload"
* if you start a new service run: pm2 start --name upload ./uploadService.js --log-date-format="YYYY-MM-DD HH:mm Z"



 
