const axios = require('axios');
var express = require("express");
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var multer = require("multer");
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./properties.file');
var app = express();
var http = null;
// Properties
if (properties.get('ssl')) {
    http = require("https");
} else {
    http = require("http");
}
var searchServiceAPIUrl = properties.get('searchServiceAPIUrl');
var monitorServiceAPIUrl = properties.get('monitorAPIUrl');
var irishMonitorServiceAPIUrl = properties.get('irishMonitorAPIUrl');
var fundersServiceAPIUrl = properties.get('fundersServiceAPIUrl');
var auth = properties.get('userInfoUrl');
/** @deprecated*/
var authDeprecated = auth.includes("accessToken");
var localPath = properties.get('localPath');
var maxSize = properties.get('max.size') * 1024;
var bigMaxSize = properties.get('big-max.size') * 1024;
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        if (req.params.id) {
            cb(null, req.params.id + (req.params.label?('-' + req.params.label):'') +
                '-' + new Date().getTime() + '.' + file.originalname.split('.').pop());
        } else {
            cb(null, file.originalname);
        }
    }
});

var upload = multer({storage: storage})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-xsrf-token');
    res.header('Access-Control-Allow-Credentials', "true");
    res.header('Access-Control-Allow-Methods', 'GET, OPTIONS, POST, DELETE');
    res.header('Access-Control-Max-Age', "1800");
    next();
});

app.get('/download/:filename', function (req, res) {
    res.download('./uploads/' + req.params.filename);
});

app.post("/upload", upload.array("uploads[]", 12), function (req, res) {
    var filepath = (localPath ? "." : __dirname) + "/" + req.files[0].path;
    let type = req.query.type;
    if (type === 'json' && req.files[0].mimetype !== 'application/json') {
        console.error("No proper file type");
        res.status(500).send(getResponse(500, "No proper file type"));
    } else if ((!type || type === 'csv') && req.files[0].mimetype !== 'text/csv' && req.files[0].mimetype !== 'application/vnd.ms-excel') {
        console.error("No proper file type");
        res.status(500).send(getResponse(500, "No proper file type"));
    } else {
        res.download(filepath);
        setTimeout(function () {
            deleteFile(filepath);
        }, 3000);
//        deleteFile(filepath);
    }

});

app.post(['/upload/:id', '/upload/stakeholder/:id', '/upload/:type/:id', '/upload/:type/:id/:label'], upload.single('photo'), (req, res) => {
    let fileMaxSize = (req.query.big)?bigMaxSize:maxSize
    sendFile(req, res, fileMaxSize, (result) => {
        let type = req.params['type'];
        let id = req.params['id'];
        let roles = result.roles;
        if(type) {
            return isPortalAdmin(roles) || isCurator(type, roles) || isManager(type, id, roles);
        } else {
            return result.sub.indexOf(id) !== -1 || isPortalAdmin(roles) || isAnyCurator(roles);
        }
    });
});

app.delete(['/delete/:filename', '/delete/stakeholder/:filename', '/delete/:type/:id/:filename'], function (req, res) {
    deleteFileSend(req, res, (result) => {
        let roles = result.roles;
        let type = req.params['type'];
        let id = req.params['id'];
        if(type && id) {
            return isPortalAdmin(roles) || isCurator(type, roles) || isManager(type, id, roles);
        } else {
            return result.sub.indexOf(req.params.filename.split('-')[0]) !== -1 || isPortalAdmin(roles) || isAnyCurator(roles);
        }
    });
});

app.get('/explore/home', async function (req, res) {
    try {
      let openOrgsId = 'openaire____%3A%3A0362fcdb3076765d9c0041ad331553e8';
      // Make requests to multiple APIs
        let requests= [
            "http://localhost:" + properties.get('port') + "/portals/countResults",
            searchServiceAPIUrl +'results/?fields=relfunder&sf=relfunder&format=json&size=0',
            searchServiceAPIUrl + 'datasources/count?format=json',
            searchServiceAPIUrl + 'resources2/?format=json&size=0&type=organizations&fq=(reldatasourcecompatibilityid exact driver or reldatasourcecompatibilityid exact driver-openaire2.0 or reldatasourcecompatibilityid exact openaire2.0 or reldatasourcecompatibilityid exact openaire3.0 or reldatasourcecompatibilityid exact openaire4.0 or reldatasourcecompatibilityid exact openaire-cris_1.1 or reldatasourcecompatibilityid exact openaire2.0_data or reldatasourcecompatibilityid exact hostedBy or relproject=* or reldatasourcecompatibilityid = native'+(openOrgsId ? (' or collectedfromdatasourceid="'+openOrgsId+'"') : "")+')',
            searchServiceAPIUrl + 'projects/?fields=funder&sf=funder&format=json&size=0',
            searchServiceAPIUrl + 'resources?query=(%20(oaftype%20exact%20result)%20and%20(resulttypeid%20exact%20dataset)%20and%20(relresulttype%3Dpublication)%20%20)&page=0&size=0&format=json',
            searchServiceAPIUrl + 'resources?query=(%20(oaftype%20exact%20result)%20and%20(resulttypeid%20exact%20software)%20and%20(relresulttype%3Dpublication)%20%20)&page=0&size=0&format=json'
        ];
        const dataPromises = requests.map((url) => axios.get( url));
        const dataResponses = await Promise.all(dataPromises);
        // Determine if all additional requests were successful
        const allRequestsSuccessful = dataResponses.every((response) => response.status === 200);
        // Combine the data
        const aggregatedData = {
            publications: dataResponses[0].data.publications,
            datasets: dataResponses[0].data.datasets,
            software: dataResponses[0].data.software,
            other: dataResponses[0].data.other,
            results: dataResponses[1].data.meta.total,
            datasources: dataResponses[2].data.total,
            organizations: dataResponses[3].data.meta.total,
            projects:dataResponses[4].data.meta.total,
            funders: parseNoOfFunders(dataResponses[1].data, dataResponses[4].data),
            datasetsInterlinked:dataResponses[5].data.meta.total,
            softwareInterlinked:dataResponses[6].data.meta.total,
            success:allRequestsSuccessful,
            calculated_at: (new Date()).toUTCString()

        };
        // Send the aggregated data as the response
        res.status(allRequestsSuccessful?200:207).json(aggregatedData);
    } catch (error) {
        console.error('Error fetching data:', error);
        res.status(500).send('Internal Server Error');
    }
});

app.get('/explore/search', async function (req, res) {
    let  aggregatedData = {};
    try {
        let openOrgsId = 'openaire____%3A%3A0362fcdb3076765d9c0041ad331553e8';
        // Make requests to multiple APIs
        let requests= [
        searchServiceAPIUrl +'resources2/?format=json&size=0&type=results',
        searchServiceAPIUrl + 'datasources/count?format=json',
        searchServiceAPIUrl + 'resources2/?format=json&size=0&type=organizations&fq=(reldatasourcecompatibilityid exact driver or reldatasourcecompatibilityid exact driver-openaire2.0 or reldatasourcecompatibilityid exact openaire2.0 or reldatasourcecompatibilityid exact openaire3.0 or reldatasourcecompatibilityid exact openaire4.0 or reldatasourcecompatibilityid exact openaire-cris_1.1 or reldatasourcecompatibilityid exact openaire2.0_data or reldatasourcecompatibilityid exact hostedBy or relproject=* or reldatasourcecompatibilityid = native' + (openOrgsId ? (' or collectedfromdatasourceid="'+openOrgsId+'"') : "") +')',
        searchServiceAPIUrl + 'projects/count?format=json&fq=projectcode<>"unidentified"'
        ]
        const dataPromises = requests.map((url) => axios.get( url));
        const dataResponses = await Promise.all(dataPromises);
        // Determine if all additional requests were successful
        const allRequestsSuccessful = dataResponses.every((response) => response.status === 200);
        // Combine the data
        aggregatedData = {
            results: dataResponses[0].data.meta.total,
            datasources: dataResponses[1].data.total,
            organizations: dataResponses[2].data.meta.total,
            projects:dataResponses[3].data.total,
            success:allRequestsSuccessful,
            calculated_at: (new Date()).toUTCString()

        };

        // Send the aggregated data as the response
        res.status(allRequestsSuccessful?200:207).json(aggregatedData);
    } catch (error) {
        console.log(aggregatedData)
        console.error('Error fetching data:', error);
        res.status(500).send('Internal Server Error');
    }
});

app.get('/portals/countResults', async function (req, res) {
    const field = req.query.field;
    let allowedFields = ["communityid"];
    if(field && allowedFields.indexOf(field) ==-1){
        res.status(405).send('Not allowed request');
        return;
    }
    const value = req.query.value;
    const params = field && value?(encodeURI("&fq=" + field + "=" + value)):"";
    let  aggregatedData = {};

    try {
        // Make requests to multiple APIs
        let requests= [
            searchServiceAPIUrl +'publications/count?format=json' + params,
            searchServiceAPIUrl +'datasets/count?format=json' + params,
            searchServiceAPIUrl +'software/count?format=json' + params,
            searchServiceAPIUrl +'other/count?format=json' + params
        ]
        const dataPromises = requests.map((url) => axios.get( url));
        const dataResponses = await Promise.all(dataPromises);
        // Determine if all additional requests were successful
        const allRequestsSuccessful = dataResponses.every((response) => response.status === 200);
        // Combine the data
        aggregatedData = {
            publications: dataResponses[0].data.total,
            datasets: dataResponses[1].data.total,
            software: dataResponses[2].data.total,
            other:dataResponses[3].data.total,
            success:allRequestsSuccessful,
            calculated_at: (new Date()).toUTCString()

        };

        // Send the aggregated data as the response
        res.status(allRequestsSuccessful?200:207).json(aggregatedData);
    } catch (error) {
        console.log(aggregatedData)
        console.error('Error fetching data:', error);
        res.status(500).send('Internal Server Error');
    }
});
app.get('/explore/funders', async function (req, res) {
    let  aggregatedData = {};
    try {
        // Make requests to multiple APIs
        let requests= [
            searchServiceAPIUrl + 'resources2/?format=json&type=results&fq=relfunder=*&refine=true&fields=relfunder&sf=relfunder&page=0&size=0',
            searchServiceAPIUrl + 'resources2/?format=json&type=results&fq=relfunder=*&refine=true&fields=relfunder&sf=relfunder&page=0&size=0&fq=resultbestaccessright%20exact%20%22Open%20Access%22',
            searchServiceAPIUrl + 'resources2/?format=json&type=projects&fq=funder=*&fq=projectcode<>"unidentified"&refine=true&fields=funder&sf=funder&page=0&size=0',
            monitorServiceAPIUrl + 'stakeholder?type=funder',
            irishMonitorServiceAPIUrl + 'stakeholder?type=funder',
            fundersServiceAPIUrl
        ]
        const dataPromises = requests.map((url) => axios.get( url));
        const dataResponses = await Promise.all(dataPromises);
        // Determine if all additional requests were successful
        const allRequestsSuccessful = dataResponses.every((response) => response.status === 200);
        let fundersMap = new Map();

        let resultsFunders = dataResponses[0].data.refineResults.relfunder;
        resultsFunders.forEach(queriedFunder => {
          let id = queriedFunder.id;
          let name = queriedFunder.name.split("||");
          if (!fundersMap.has(id)) {
                fundersMap.set(id,{name: name?.length>0 ? name[0] : "", shortName: name?.length>1 ? name[1] : "", id: id, results:queriedFunder.count, openResults: null,  projects:null, stakeholder:null});
            }
        });
        let openResultsFunders = dataResponses[1].data.refineResults.relfunder;
        openResultsFunders.forEach(queriedFunder => {
          let id = queriedFunder.id;
          let name = queriedFunder.name.split("||");
          if (!fundersMap.has(id)) {
                fundersMap.set(id,{name: name?.length>0 ? name[0] : "", shortName: name?.length>1 ? name[1] : "", id: id, results:null, openResults: queriedFunder.count,  projects:null, stakeholder:null});
            }else{
                fundersMap.get(id).openResults = queriedFunder.count;
            }
        });
        let projectFunders = dataResponses[2].data.refineResults.funder;
        projectFunders.forEach(queriedFunder => {
          let id = queriedFunder.id;
          let name = queriedFunder.name.split("||");
          if (!fundersMap.has(id) ) {
                fundersMap.set(id,{name: name?.length>0 ? name[0] : "", shortName: name?.length>1 ? name[1] : "", id: id, results:null, openResults: null,  projects:queriedFunder.count, stakeholder:null});
            }else{
                fundersMap.get(id).projects = queriedFunder.count;
            }
        });
        let stakeholders = dataResponses[3].data;
        stakeholders.forEach(stakeholder => {
            let id = stakeholder.index_id + "||" + stakeholder.index_name + "||" + stakeholder.index_shortName;
            if (fundersMap.has(id) && (!fundersMap.get(id).stakeholder || (fundersMap.get(id).name === stakeholder.name) || (fundersMap.get(id).shortName === stakeholder.index_shortName))) {
              let ministakeholder = {
                  id:id, name:stakeholder.name,
                  alias: stakeholder.alias, visibility: stakeholder.visibility,
                  irishAlias: '', irishVisibility: '',
                  logoUrl:stakeholder.logoUrl, isUpload: stakeholder.isUpload, websiteUrl: null}
              fundersMap.get(id).stakeholder = ministakeholder;

              if(!fundersMap.get(id).shortName) {
                fundersMap.get(id).shortName = stakeholder.index_shortName;
              }
            }
        });

      let irishStakeholders = dataResponses[4].data;
      irishStakeholders.forEach(stakeholder => {
        let id = stakeholder.index_id + "||" + stakeholder.index_name + "||" + stakeholder.index_shortName;
        if (fundersMap.has(id) && (!fundersMap.get(id).stakeholder || (fundersMap.get(id).name === stakeholder.name) || (fundersMap.get(id).shortName === stakeholder.index_shortName))) {
          let ministakeholder = {
            id:id, name:stakeholder.name,
            alias: fundersMap.get(id).stakeholder ? fundersMap.get(id).stakeholder.alias : '',
            visibility: fundersMap.get(id).stakeholder ? fundersMap.get(id).stakeholder.visibility : '',
            irishAlias: stakeholder.alias, irishVisibility: stakeholder.visibility,
            logoUrl:stakeholder.logoUrl, isUpload: stakeholder.isUpload, websiteUrl: null}
          fundersMap.get(id).stakeholder = ministakeholder;

          if(!fundersMap.get(id).shortName) {
            fundersMap.get(id).shortName = stakeholder.index_shortName;
          }
        }
      });

      let funders = dataResponses[5].data;
      funders.forEach(funder => {
        // let id = funder.id + "||" + (funder.legalName ? (funder.legalName + "||" + (funder.legalShortName ? funder.legalShortName : "")) : "");
        // if (fundersMap.has(id)) {
        //   let storedFunder = fundersMap.get(id);
        let storedFunder = null;
        fundersMap.forEach((value, key, map) => {
           if(key.split("||")[0] === funder.id) {
             storedFunder = value;
           }
        })
        if (storedFunder) {
          // if(funder.legalName && storedFunder.name && funder.legalName != storedFunder.name) {
          //   console.log("Diffrent name!!! index: "+storedFunder.name + " vs funders API: "+funder.legalName);
          // }
          if(funder.legalName) {
            storedFunder.name = funder.legalName;
          }
          storedFunder.country = funder.country;
          storedFunder.registered = funder.registered;
          if(!storedFunder.shortName) {
            storedFunder.shortName = funder.legalShortName;
          }
          let storedStakeholder = storedFunder.stakeholder;

          // if(funder.legalShortName && storedFunder.shortName && funder.legalShortName != storedFunder.shortName) {
          //   console.log("Diffrent short name!!! index: "+storedFunder.shortName + " vs funders API: "+funder.legalShortName);
          // }

          let ministakeholder = {
            id:storedFunder.id, name:storedStakeholder?.name,
            alias: storedStakeholder?.alias ? storedStakeholder.alias : funder.legalShortName,
            visibility: storedStakeholder?.visibility,
            irishAlias: storedStakeholder?.irishAlias ? storedStakeholder.irishAlias : funder.legalShortName,
            irishVisibility: storedStakeholder?.irishVisibility,
            websiteUrl: funder.websiteUrl, logoUrl: storedStakeholder?.logoUrl ? storedStakeholder.logoUrl : funder.logoUrl,
            isUpload: storedStakeholder?.isUpload
          };
          storedFunder.stakeholder = ministakeholder;
        // } else {
        //   console.log(funder.id);
        }
      });

      // console.log("")
      // for(let f of fundersMap.keys()) {
      //   if(!fundersMap.get(f).stakeholder || (!fundersMap.get(f).stakeholder.websiteUrl && !fundersMap.get(f).country && !fundersMap.get(f).registered)) {
      //     console.log(f);
      //   }
      // }

        // Combine the data

        // Send the aggregated data as the response
        // console.log(fundersMap)
        aggregatedData = {
            count: fundersMap.size,
            results: dataResponses[0].data.meta.total,
            projects: dataResponses[2].data.meta.total,
            funders: Array.from(fundersMap.values()),
            success:allRequestsSuccessful,
            calculated_at: (new Date()).toUTCString()

        };
        res.status(allRequestsSuccessful?200:207).json(aggregatedData);
    } catch (error) {
        // console.log(aggregatedData)
        console.error('Error fetching data:', error);
        res.status(500).send('Internal Server Error');
    }
});
app.get('/grouped-requests', async function (req, res) {
        res.json([
            "/explore/search",
            "/explore/home",
          "/explore/funders"
        ]);
});
function parseNoOfFunders(resultRES, projectsRES){
    // combines the refines qeries on funders field, the funders with results and the funders that have at least one project
    let mergedFundersSet = new Set();
    let queriedFunders = resultRES.refineResults.relfunder;
    queriedFunders.forEach(queriedFunder => {
        if (!mergedFundersSet.has(queriedFunder.id)) {
            mergedFundersSet.add(queriedFunder.id);
        }
    });
    queriedFunders = projectsRES.refineResults.funder;
    queriedFunders.forEach(queriedFunder => {
        if(+queriedFunder.count > 1) {
            if (!mergedFundersSet.has(queriedFunder.id)) {
                mergedFundersSet.add(queriedFunder.id);
            }
        }
    });
    return mergedFundersSet.size;
}


const server = app.listen(properties.get('port'), function () {
    console.log("Listening on port %s...", server.address().port);
});

function sendFile(req, res, size, authorized) {
    const cookie = (authDeprecated)?req.cookies['AccessToken']:req.cookies['openAIRESession'];
    const file = req.file;
    var filepath = (localPath ? "." : __dirname) + "/" + file.path;
    if (!cookie) {
        res.status(401).send(getResponse(401, "Unauthorized"));
        deleteFile(filepath);
    } else if (!file || (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png')) {
        res.status(500).send(getResponse(500, "No image file type"));
        deleteFile(filepath);
    } else if (file.size > size) {
        res.status(500).send(getResponse(500, "Exceeds file size limit"));
        deleteFile(filepath);
    } else {
        getUserInfo(authorized, req, res, () => {
            res.send(file);
        });
    }
}

function deleteFileSend(req, res, authorized) {
    const cookie = (authDeprecated)?req.cookies['AccessToken']:req.cookies['openAIRESession'];
    if (!cookie) {
        res.status(401).send(getResponse(401, "Unauthorized"));
    } else {
        getUserInfo(authorized, req, res, () => {
            deleteFile('./uploads/' + req.params.filename, res);
        });
    }
}

function getUserInfo(authorized, req, res, success) {
    let url = (authDeprecated)?(auth + cookie):auth;
    http.get(url, {headers: {Cookie: req.header('Cookie')}}, function (resp) {
        var responseString = "";
        resp.on("data", function (data) {
            responseString += data;
        });
        resp.on("end", function () {
            var result = JSON.parse(responseString);
            if (result.error) {
                res.status(401).send(getResponse(401, "Unauthorized"));
            } else {
                if (authorized(result)) {
                    success();
                } else {
                    res.status(401).send(getResponse(401, "Unauthorized"));
                }
            }
        });
    });
}

function getResponse(code, message) {
    var response = {};
    response["code"] = code;
    response["message"] = message;
    return response;
}

function deleteFile(filepath, res = null) {
    const fs = require('fs');
    fs.stat(filepath, function (err, stats) {
        if (err) {
            return console.error(err);
        }
        try{
            fs.unlinkSync(filepath);
            console.log('file deleted successfully');
            if(res) {
                res.send("File Deleted Successfully");
            }
        } catch (err) {
           console.error(err);
        }
    });
}

function mapType(type, communityMap = true) {
    if(type === 'organization') {
        return 'institution';
    } else if(type === 'ri' && communityMap) {
        return 'community';
    } else {
        return type;
    }
}

function isPortalAdmin(roles) {
    return roles.includes("PORTAL_ADMINISTRATOR");
}

function isAnyCurator(roles) {
    return roles.filter(role => role.includes("CURATOR_")).length > 0;
}

function isCurator(type, roles) {
    return roles.includes("CURATOR_" + mapType(type).toUpperCase());
}

function isManager(type, id, roles) {
    return roles.includes(mapType(type).toUpperCase() + "_" + id.toUpperCase() + "_MANAGER");
}
